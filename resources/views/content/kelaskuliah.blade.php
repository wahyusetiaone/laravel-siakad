@extends('layouts.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Kelas Kuliah</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Kelas Kuliah</a></li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <h3 class="card-title">
                                    <a class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal-tambah">
                                        <i class="fas fa-pencil-alt">
                                        </i>
                                        Tambah Kelas kuliah
                                    </a>
                                </h3>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="jenis_matakuliah">Program Studi</label>
                                        <select class="form-control" id="s_kode_prodi" name="kode_prodi">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="jenis_matakuliah">Semester</label>
                                        <select class="form-control" id="s_semester" name="semester">
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div id="tb">
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Modal Start -->
    <div class="modal fade" id="modal-tambah">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Kelas Kuliah</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" id="form-tambah">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <!-- form start -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="id_kelas_kuliah">ID Kelas Kuliah</label>
                                        <input type="text" class="form-control" id="t_id_kelas_kuliah" name="id_kelas_kuliah" placeholder="ID Kelas Kuliah">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="kode_prodi">Program Studi</label>
                                        <select class="form-control" id="t_kode_prodi" name="kode_prodi">
                                            @foreach($pr as $p)
                                                <option value="{{$p->kode_prodi}}" >{{$p->nama_prodi}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="semester">Semester</label>
                                        <input type="number" class="form-control" id="t_semester" name="semester" placeholder="Semester">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="kode_matakuliah">Matakuliah</label>
                                        <select class="form-control" id="t_kode_matakuliah" name="kode_matakuliah">
                                            @foreach($mtk as $m)
                                                <option value="{{$m->kode_matakuliah}}" >{{$m->nama_matakuliah}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="nama_kelas">Nama Kelas</label>
                                        <input type="text" class="form-control" id="t_nama_kelas" name="nama_kelas" placeholder="Nama Kelas">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Tambah Prodi</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- Modal Start -->
    <div class="modal fade" id="modal-edit">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Kelas Kuliah</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" id="form-edit">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <!-- form start -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="id_kelas_kuliah">ID Kelas Kuliah</label>
                                        <input type="text" class="form-control" id="e_id_kelas_kuliah" name="id_kelas_kuliah" placeholder="ID Kelas Kuliah" readonly="true">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="kode_prodi">Program Studi</label>
                                        <select class="form-control" id="e_kode_prodi" name="kode_prodi">
                                            @foreach($pr as $p)
                                                <option value="{{$p->kode_prodi}}" >{{$p->nama_prodi}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="semester">Semester</label>
                                        <input type="number" class="form-control" id="e_semester" name="semester" placeholder="Semester">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="no_sk_prodi">kode_matakuliah</label>
                                        <select class="form-control" id="e_kode_matakuliah" name="kode_matakuliah">
                                            @foreach($mtk as $m)
                                                <option value="{{$m->kode_matakuliah}}" >{{$m->nama_matakuliah}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="nama_kelas">Nama Kelas</label>
                                        <input type="text" class="form-control" id="e_nama_kelas" name="nama_kelas" placeholder="Nama Kelas">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Edit Prodi</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div class="modal fade" id="modal-hapus">
        <div class="modal-dialog modal-md">
            <form role="form" id="form-hapus">
                {{ csrf_field() }}
                <div class="modal-content">
                    <for class="modal-body">
                        <div class="modal-header">
                            <h4 class="modal-title">Hapus Kelas Kuliah</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Yakin ingin menghapus Kode <span id="h_span_id_kelas_kuliah"></span> dengan nama <span id="h_span_nama_matakuliah"></span> ? Menghapus data juga akan menghapus semua record data yang terkait dengan data tersebut !!!</p>
                            <input type="hidden" id="h_id_kelas_kuliah" name="id_kelas_kuliah">
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Ya, Yakin</button>
                        </div>
                </div>
            </form>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

@endsection

@section('javascript')
    <!-- jQuery -->
    <script src="/dist/plugins/jquery/jquery.min.js"></script>
    <!-- DataTables -->
    <script src="/dist/plugins/datatables/jquery.dataTables.js"></script>
    <script src="/dist/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
    <!-- SweetAlert2 -->
    <script src="/dist/plugins/sweetalert2/sweetalert2.min.js"></script>
    <!-- Toastr -->
    <script src="/dist/plugins/toastr/toastr.min.js"></script>
    <!-- AdminLTE App -->
    <script src="/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="/dist/js/demo.js"></script>
    <!-- page script -->
    <script>

        var newProdi = {"All":"all"};
        var newSemester = {"All":"all"};

        $( document ).ready(function() {
            fistTimeOptions();
        });

        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
        });

        //triggered when modal is about to be shown
        $('#modal-edit').on('show.bs.modal', function(e) {

            //get data-id attribute of the clicked element
            var id_kelas_kuliah = $(e.relatedTarget).data('id-kelas-kuliah');
            $.ajax({
                url:'kelaskuliah/get',
                type:'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data:{
                    'id_kelas_kuliah' : id_kelas_kuliah
                },
                dataType: 'json',
                success: function (response) {
                    var kk = response.data.return;
                    $('#e_id_kelas_kuliah').val(kk[0]);
                    $('#e_kode_prodi option[value="'+kk[1]+'"]').prop('selected', true);
                    $('#e_semester').val(kk[4]);
                    $('#e_kode_matakuliah option[value="'+kk[5]+'"]').prop('selected', true);
                    $('#e_nama_kelas').val(kk[7]);
                },
            });
        }).submit(function (e) {
            e.preventDefault();
            $.ajax({
                url:'kelaskuliah/edit',
                type:'post',
                data:$('#form-edit').serialize(),
                success:function(){
                    $('#modal-edit').modal('hide');
                    //for backdrop not remove
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    Toast.fire({
                        type: 'success',
                        title: 'Berhasil edit Program Studi !!!'
                    });
                    resetFactoryTable();
                }
            });
        })

        $('#form-tambah').submit(function(e){
            e.preventDefault();
            $.ajax({
                url:'kelaskuliah/tambah',
                type:'post',
                data:$('#form-tambah').serialize(),
                success:function(){
                    $('#modal-tambah').modal('hide');
                    //for backdrop not remove
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    Toast.fire({
                        type: 'success',
                        title: 'Program Studi Berhasil ditambahkan !!!'
                    });
                    resetFactoryTable();
                }
            });
        });

        $('#modal-hapus').on('show.bs.modal', function(e) {
            var id_kelaskuliah = $(e.relatedTarget).data('id-kelas-kuliah');
            var nama_matakuliah = $(e.relatedTarget).data('nama-matakuliah');
            $('#h_span_id_kelas_kuliah').text(id_kelaskuliah);
            $('#h_span_nama_matakuliah').text(nama_matakuliah);
            $('#h_id_kelas_kuliah').val(id_kelaskuliah);
        }).submit(function(e){
            e.preventDefault();
            $.ajax({
                url:'kelaskuliah/hapus',
                type:'post',
                data:$('#form-hapus').serialize(),
                success:function(){
                    $.ajax({
                        url:'nilai/hapus-use-kelaskuliah',
                        type:'post',
                        data:$('#form-hapus').serialize(),
                        success:function() {
                            $('#modal-hapus').modal('hide');
                            //for backdrop not remove
                            $('body').removeClass('modal-open');
                            $('.modal-backdrop').remove();
                            Toast.fire({
                                type: 'success',
                                title: 'Program Studi Berhasil dihapus !!!'
                            });
                            resetFactoryTable();
                            }
                        });
                    // $('#modal-hapus').modal('hide');
                    // //for backdrop not remove
                    // $('body').removeClass('modal-open');
                    // $('.modal-backdrop').remove();
                    // Toast.fire({
                    //     type: 'success',
                    //     title: 'Program Studi Berhasil dihapus !!!'
                    // });
                    // resetFactoryTable();
                }
            });
        });

        function fistTimeOptions() {

            @foreach($kk as $k)
                newProdi["{{$k->nama_prodi}}"] = "{{$k->kode_prodi}}";
                newSemester["{{$k->semester}}"] = "{{$k->semester}}";
            @endforeach

            var $prodi = $("#s_kode_prodi");
            $prodi.empty(); // remove old options
            $.each(newProdi, function(key,value) {
                $prodi.append($("<option></option>")
                    .attr("value", value).text(key));
            });

            resetFactoryTable();

        }

        function resetFactoryTable(){
            $('#s_kode_prodi option[value="all"]').prop('selected', true);

            var $semester = $("#s_semester");
            $semester.empty(); // remove old options
            $.each(newSemester, function(key,value) {
                $semester.append($("<option></option>")
                    .attr("value", value).text(key));
            });
            $('#s_semester option[value="all"]').prop('selected', true);

            // $('#body').load("kelaskuliah/tampilQuery/sistem%20informatika/3");
            $('#tb').load("kelaskuliah/tampilQuery/all/all");
        }

        $('#s_kode_prodi').on('change', function() {
            var kode_prodi = $('#s_kode_prodi').val();
            var semester = $('#s_semester').val();
            $('#tb').load("kelaskuliah/tampilQuery/"+kode_prodi+"/"+semester);
        });

        $('#s_semester').on('change', function() {
            var kode_prodi = $('#s_kode_prodi').val();
            var semester = $('#s_semester').val();
            $('#tb').load("kelaskuliah/tampilQuery/"+kode_prodi+"/"+semester);
        });

    </script>
@stop
