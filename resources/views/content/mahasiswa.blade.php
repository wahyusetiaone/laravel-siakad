@extends('layouts.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Contacts</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Contacts</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="card card-solid">
                <div class="card-body pb-0">
                    <div class="row">
                        <div class="col-md-12">
                            <a class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal-tambah">
                                <i class="fas fa-pencil-alt">
                                </i>
                                Tambah Mahasiswa
                            </a>
                        </div>
                    </div>
                    <br>
                    <div class="row d-flex align-items-stretch">
                        @foreach($mhs as $m)
                        <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
                            <div class="card bg-light">
                                <div class="card-header text-muted border-bottom-0">
                                </div>
                                <div class="card-body pt-0">
                                    <div class="row">
                                        <div class="col-7">
                                            <h2 class="lead"><b>{{$m->nama}}</b></h2>
                                            <p class="text-muted text-sm"><b>Tempat,tgl lahir: </b> {{$m->tempat_lahir}}</p>
                                            <ul class="ml-4 mb-0 fa-ul text-muted">
                                                <li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span> Alamat: {{$m->jalan}} {{$m->rt}}/{{$m->rw}}, {{$m->dusun}}, {{$m->kelurahan}}, {{$m->kecamatan}} Kode Pos : {{$m->kode_pos}}</li>
                                                <li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> Phone #: {{$m->hp}}</li>
                                            </ul>
                                        </div>
                                        <div class="col-5 text-center">
                                            <img src="../../dist/img/user1-128x128.jpg" alt="" class="img-circle img-fluid">
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="text-right">
                                        <a href="#" class="btn btn-sm bg-gradient-red" data-toggle="modal" data-nik="{{$m->nik}}" data-nama="{{$m->nama}}" data-target="#modal-hapus">
                                            <i class="fas fa-trash"></i>
                                        </a>
                                        <a href="{{ url('/mahasiswa/details/'.$m->nik) }}" class="btn btn-sm btn-primary">
                                            <i class="fas fa-user"></i> View Profile
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <nav aria-label="Contacts Page Navigation">
                        <ul class="pagination justify-content-center m-0">
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">4</a></li>
                            <li class="page-item"><a class="page-link" href="#">5</a></li>
                            <li class="page-item"><a class="page-link" href="#">6</a></li>
                            <li class="page-item"><a class="page-link" href="#">7</a></li>
                            <li class="page-item"><a class="page-link" href="#">8</a></li>
                        </ul>
                    </nav>
                </div>
                <!-- /.card-footer -->
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Modal Start -->
    <div class="modal fade" id="modal-tambah">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Mahasiswa</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header p-2">
                                    <ul class="nav nav-pills">
                                        <li class="nav-item">
                                            <a class="nav-link active" href="#datadiri"data-toggle="tab">Data Diri</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#pendidikan" data-toggle="tab">Pendidikan</a>
                                        </li>
                                    </ul>
                                </div><!-- /.card-header -->
                                <div class="card-body">
                                    <div class="tab-content">
                                        <div class="active tab-pane" id="datadiri">
                                            <div class="row">
                                                <div class="col-12">
                                                    <form id="form-tambah-mhs">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label for="nik">NIK</label>
                                                                    <input type="text" class="form-control" id="nik" name="nik" placeholder="NIK">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label for="nisn">NISN</label>
                                                                    <input type="text" class="form-control" id="nisn" name="nisn" placeholder="NISN">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label for="npwp">NPWP</label>
                                                                    <input type="text" class="form-control" id="npwp" name="npwp" placeholder="NPWP">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label for="nama">Nama Mahasiswa</label>
                                                                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Mahasiswa">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label for="tanggal_lahir">Tanggal Lahir</label>
                                                                    <input type="date" class="form-control" id="tanggal_lahir" name="tanggal_lahir" placeholder="Tanggal Lahir">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label for="tempat_lahir">Tempat Lahir</label>
                                                                    <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" placeholder="Tempat Lahir">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label for="jenis_kelamin">Jenis Kelamin</label>
                                                                    <select class="form-control" id="jenis_kelamin" name="jenis_kelamin">
                                                                        <option value="L">Laki - laki</option>
                                                                        <option value="P">Perempuan</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label for="agama">Agama</label>
                                                                    <input type="text" class="form-control" id="agama" name="agama" placeholder="Agama">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label for="nama_ibu_kandung">Nama Ibu Kandung</label>
                                                                    <input type="text" class="form-control" id="nama_ibu_kandung" name="nama_ibu_kandung" placeholder="Nama Ibu">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label for="kewarganegaraan">Kewarganegaraan</label>
                                                                    <input type="text" class="form-control" id="kewarganegaraan" name="kewarganegaraan" placeholder="Kewarganegaraan">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-2">
                                                                <div class="form-group">
                                                                    <label for="rt">RT</label>
                                                                    <input type="number" class="form-control" id="rt" name="rt" placeholder="RT">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <div class="form-group">
                                                                    <label for="rw">RW</label>
                                                                    <input type="number" class="form-control" id="rw" name="rw" placeholder="RW">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <div class="form-group">
                                                                    <label for="jalan">Jalan</label>
                                                                    <input type="text" class="form-control" id="jalan" name="jalan" placeholder="Jalan">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <div class="form-group">
                                                                    <label for="dusun">Dusun</label>
                                                                    <input type="text" class="form-control" id="dusun" name="dusun" placeholder="Dusun">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-5">
                                                                <div class="form-group">
                                                                    <label for="kelurahan">Kelurahan</label>
                                                                    <input type="text" class="form-control" id="kelurahan" name="kelurahan" placeholder="Kelurahan">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-5">
                                                                <div class="form-group">
                                                                    <label for="kecamatan">Kecamatan</label>
                                                                    <input type="text" class="form-control" id="kecamatan" name="kecamatan" placeholder="Kecamatan">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <div class="form-group">
                                                                    <label for="kode_pos">Kode Pos</label>
                                                                    <input type="text" class="form-control" id="kode_pos" name="kode_pos" placeholder="Kode Pos">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <div class="form-group">
                                                                    <label for="telepon"> Telepon </label>
                                                                    <input type="text" class="form-control" id="telepon" name="telepon" placeholder="Telepon">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <div class="form-group">
                                                                    <label for="hp">HP</label>
                                                                    <input type="text" class="form-control" id="hp" name="hp" placeholder="HP">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <div class="form-group">
                                                                    <label for="email">Email</label>
                                                                    <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <!-- /.col -->
                                                </div>
                                                <!-- /.row -->
                                            </div>
                                        </div>

                                        <div class="tab-pane" id="pendidikan">
                                            <div class="row">
                                                <div class="col-12">
                                                    <form role="form" id="form-tambah-pendidikan">
                                                        {{ csrf_field() }}
                                                            <!-- form start -->
                                                            <div class="card-body">
                                                                <div class="row">
                                                                    <div class="col-sm-6">
                                                                        <div class="form-group">
                                                                            <label for="nim">NIM</label>
                                                                            <input type="number" class="form-control" id="nim" name="nim" placeholder="NIM">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <div class="form-group">
                                                                            <label for="nik">NIK</label>
                                                                            <input type="number" class="form-control" id="p-nik" name="nik" placeholder="NIK" readonly>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-sm-4">
                                                                        <div class="form-group">
                                                                            <label for="jenis_pendaftaran">Jenis Pendaftaran</label>
                                                                            <select class="form-control" id="jenis_pendaftaran" name="jenis_pendaftaran">
                                                                                <option value="baru" >Baru</option>
                                                                                <option value="pindahan" >Pindah</option>
                                                                                <option value="transfer" >Transfer</option>
                                                                                <option value="alih jenjang" >Alih Jenjang</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-4">
                                                                        <div class="form-group">
                                                                            <label for="jalur_pendaftaran">Jalur Pendaftaran</label>
                                                                            <select class="form-control" id="jalur_pendaftaran" name="jalur_pendaftaran">
                                                                                <option value="mandiri" >Mandiri</option>
                                                                                <option value="beasiswa" >Beasiswa</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-4">
                                                                        <div class="form-group">
                                                                            <label for="tanggal_masuk"> Tanggal Masuk </label>
                                                                            <input type="date" class="form-control" id="tanggal_masuk" name="tanggal_masuk" placeholder="Tanggal Masuk">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-sm-6">
                                                                        <div class="form-group">
                                                                            <label for="perguruan_tinggi"> Perguruan Tinggi </label>
                                                                            <input type="text" class="form-control" id="perguruan_tinggi" name="perguruan_tinggi" placeholder="Perguruan Tinggi">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <div class="form-group">
                                                                            <label for="kode_prodi">Kode Prodi</label>
                                                                            <select class="form-control" id="kode_prodi" name="kode_prodi">
                                                                                @foreach($allprodi as $ap)
                                                                                    <option value="{{$ap->kode_prodi}}" >{{$ap->nama_prodi}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <!-- /.card-body -->
                                                        </div>
                                                    </form>
                                                    <!-- /.col -->
                                                </div>
                                                <!-- /.row -->
                                            </div>
                                        </div>
                                        <!-- /.tab-pane -->
                                    </div>
                                    <!-- /.tab-content -->
                                </div><!-- /.card-body -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="button" id="btn-add" class="btn btn-success">Tambah Matakuliah</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div class="modal fade" id="modal-hapus">
        <div class="modal-dialog modal-md">
            <form role="form" id="form-hapus">
                {{ csrf_field() }}
                <div class="modal-content">
                    <for class="modal-body">
                        <div class="modal-header">
                            <h4 class="modal-title">Hapus Mahasiswa</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Yakin ingin menghapus data dengan nik <span id="h_span_nik"></span> atasnama <span id="h_span_nama"></span> ? Menghapus data mahasiswa berarti juga menghapus rekam semua history pendidikan mahasiswa tersebut !</p>
                            <input type="hidden" id="h_nik" name="nik">
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Ya, Yakin</button>
                        </div>
                </div>
            </form>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

@endsection

@section('javascript')
    <!-- jQuery -->
    <script src="/dist/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="/dist/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="/dist/js/demo.js"></script>
    <script src="/dist/plugins/sweetalert2/sweetalert2.min.js"></script>
    <!-- Toastr -->
    <script src="/dist/plugins/toastr/toastr.min.js"></script>
    <script>
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
        });

        $('#nik').on('input', function () {
            $('#p-nik').val($('#nik').val());
        });

        $('#btn-add').click(function (e) {
            e.preventDefault();
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url:'/mahasiswa/tambah',
                type:'post',
                data:$('#form-tambah-mhs').serialize(),
                success:function(){
                    Toast.fire({
                        type: 'success',
                        title: 'Berhasil edit Mahasiswa !!!'
                    });
                }
            });
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url:'/pendidikan/tambah',
                type:'post',
                data:$('#form-tambah-pendidikan').serialize(),
                success:function(){
                    $('#modal-tambah').modal('hide');
                    Toast.fire({
                        type: 'success',
                        title: 'Berhasil edit Mahasiswa !!!'
                    });
                }
            });
        })

        $('#modal-hapus').on('show.bs.modal', function(e) {
            var nik = $(e.relatedTarget).data('nik');
            var nama = $(e.relatedTarget).data('nama');
            $('#h_span_nik').text(nik);
            $('#h_span_nama').text(nama);
            $('#h_nik').val(nik);
        }).submit(function(e){
            e.preventDefault();
            $.ajax({
                url:'mahasiswa/hapus',
                type:'post',
                data:$('#form-hapus').serialize(),
                success:function(){
                    $('#modal-hapus').modal('hide');
                }
            });
            $.ajax({
                url:'pendidikan/hapus-use-nik',
                type:'post',
                data:$('#form-hapus').serialize(),
                success:function(){
                    $('#modal-hapus').modal('hide');
                    Toast.fire({
                        type: 'success',
                        title: 'Mahasiswa Berhasil dihapus !!!'
                    });
                    $( "#tb-prodi" ).load( "prodi #tb-prodi" );
                }
            });
        });
    </script>
@stop
