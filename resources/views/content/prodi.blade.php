@extends('layouts.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Program Studi</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Program Studi</a></li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <a class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal-tambah">
                                    <i class="fas fa-pencil-alt">
                                    </i>
                                    Tambah Program Studi
                                </a>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
{{--                            tb_prodi--}}
                            <div id="tb">
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Modal Start -->
    <div class="modal fade" id="modal-tambah">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Program Studi</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" id="form-tambah">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <!-- form start -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="kode_prodi">Kode Prodi</label>
                                        <input type="text" class="form-control" id="t_kode_prodi" name="kode_prodi" placeholder="Kode Prodi">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="nama_prodi">Nama Prodi</label>
                                        <input type="text" class="form-control" id="t_nama_prodi" name="nama_prodi" placeholder="Nama Prodi">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="akreditasi">Akreditasi</label>
                                        <input type="text" class="form-control" id="t_akreditasi" name="akreditasi" placeholder="Akreditasi">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="no_sk_prodi">No SK Prodi</label>
                                        <input type="text" class="form-control" id="t_no_sk_prodi" name="no_sk_prodi" placeholder="No SK Prodi">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Tambah Prodi</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- Modal Start -->
    <div class="modal fade" id="modal-edit">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Program Studi</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" id="form-edit">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <!-- form start -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="kode_prodi">Kode Prodi</label>
                                        <input type="text" class="form-control" id="e_kode_prodi" name="kode_prodi" placeholder="Kode Prodi" readonly="true">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="nama_prodi">Nama Prodi</label>
                                        <input type="text" class="form-control" id="e_nama_prodi" name="nama_prodi" placeholder="Nama Prodi">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="akreditasi">Akreditasi</label>
                                        <input type="text" class="form-control" id="e_akreditasi" name="akreditasi" placeholder="Akreditasi">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="no_sk_prodi">No SK Prodi</label>
                                        <input type="text" class="form-control" id="e_no_sk_prodi" name="no_sk_prodi" placeholder="No SK Prodi">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Edit Prodi</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div class="modal fade" id="modal-hapus">
        <div class="modal-dialog modal-md">
            <form role="form" id="form-hapus">
                {{ csrf_field() }}
                <div class="modal-content">
                    <for class="modal-body">
                        <div class="modal-header">
                            <h4 class="modal-title">Hapus Program Studi</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Yakin ingin menghapus Kode <span id="h_span_kode_prodi"></span> dengan nama <span id="h_span_nama_prodi"></span></p>
                            <input type="hidden" id="h_kode_prodi" name="kode_prodi">
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Ya, Yakin</button>
                        </div>
                </div>
            </form>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

@endsection

@section('javascript')
    <!-- jQuery -->
    <script src="/dist/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="/dist/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- DataTables -->
    <script src="/dist/plugins/datatables/jquery.dataTables.js"></script>
    <script src="/dist/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
    <!-- SweetAlert2 -->
    <script src="/dist/plugins/sweetalert2/sweetalert2.min.js"></script>
    <!-- Toastr -->
    <script src="/dist/plugins/toastr/toastr.min.js"></script>
    <!-- AdminLTE App -->
    <script src="/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="/dist/js/demo.js"></script>
    <!-- page script -->
    <script>
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
        });

        $( document ).ready(function() {
            fn_loadtb()
        });

        //triggered when modal is about to be shown
        $('#modal-edit').on('show.bs.modal', function(e) {

            //get data-id attribute of the clicked element
            var kode_prodi = $(e.relatedTarget).data('kode-prodi');
            $.ajax({
                url:'prodi/get',
                type:'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data:{
                    'kode_prodi' : kode_prodi
                },
                dataType: 'json',
                success: function (response) {
                    var pr = response.data.return;
                    $('#e_kode_prodi').val(pr[0]);
                    $('#e_nama_prodi').val(pr[1]);
                    $('#e_akreditasi').val(pr[2]);
                    $('#e_no_sk_prodi').val(pr[3]);
                },
            });
        }).submit(function (e) {
            e.preventDefault();
            $.ajax({
                url:'prodi/edit',
                type:'post',
                data:$('#form-edit').serialize(),
                success:function(){
                    $('#modal-edit').modal('hide');
                    Toast.fire({
                        type: 'success',
                        title: 'Berhasil edit Program Studi !!!'
                    });
                    fn_loadtb();
                }
            });
        })

        $('#form-tambah').submit(function(e){
            e.preventDefault();
            $.ajax({
                url:'prodi/tambah',
                type:'post',
                data:$('#form-tambah').serialize(),
                success:function(){
                    $('#modal-tambah').modal('hide');
                    Toast.fire({
                        type: 'success',
                        title: 'Program Studi Berhasil ditambahkan !!!'
                    });
                    fn_loadtb();
                }
            });
        });

        $('#modal-hapus').on('show.bs.modal', function(e) {
            var kode_prodi = $(e.relatedTarget).data('kode-prodi');
            var nama_prodi = $(e.relatedTarget).data('nama-prodi');
            $('#h_span_kode_prodi').text(kode_prodi);
            $('#h_span_nama_prodi').text(nama_prodi);
            $('#h_kode_prodi').val(kode_prodi);
        }).submit(function(e){
            e.preventDefault();
            $.ajax({
                url:'prodi/hapus',
                type:'post',
                data:$('#form-hapus').serialize(),
                success:function(){
                    $('#modal-hapus').modal('hide');
                    Toast.fire({
                        type: 'success',
                        title: 'Program Studi Berhasil dihapus !!!'
                    });
                    fn_loadtb();
                }
            });
        });

        function fn_loadtb() {
            $( "#tb" ).load( "prodi/tb" );
        }
    </script>
@stop
