<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body>
<table id="tb-prodi" class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>Kode Prodi</th>
        <th>Nama Prodi</th>
        <th>Akreditasi</th>
        <th>No SK</th>
        <th>Aksi</th>
    </tr>
    </thead>
    <tbody>
    @foreach($pr as $p)
        <tr>
            <td>{{$p->kode_prodi}}</td>
            <td>{{$p->nama_prodi}}</td>
            <td>{{$p->akreditasi}}</td>
            <td>{{$p->no_sk_prodi}}</td>
            <td>
                <a class="btn btn-info btn-sm" data-toggle="modal" data-kode-prodi="{{$p->kode_prodi}}" data-target="#modal-edit">
                    <i class="fas fa-pencil-alt">
                    </i>
                    Edit
                </a>
                <a class="btn btn-danger btn-sm" data-toggle="modal" data-kode-prodi="{{$p->kode_prodi}}" data-nama-prodi="{{$p->nama_prodi}}" data-target="#modal-hapus">
                    <i class="fas fa-trash">
                    </i>
                    Delete
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <th>Kode Prodi</th>
        <th>Nama Prodi</th>
        <th>Akreditasi</th>
        <th>No SK</th>
        <th>Aksi</th>
    </tr>
    </tfoot>
</table>
<!-- Bootstrap 4 -->
<script src="/dist/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables -->
<script src="/dist/plugins/datatables/jquery.dataTables.js"></script>
<script src="/dist/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
    $(function () {
        $("#tb-prodi").DataTable();
    });
</script>
</body>
