<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body>
<table id="tb-matakuliah" class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>Kode Matakuliah</th>
        <th>Nama Matakuliah</th>
        <th>Kode Prodi</th>
        <th>Jenis</th>
        <th>Bobot(m/t/p)</th>
        <th>Aksi</th>
    </tr>
    </thead>
    <tbody>
    @foreach($mt as $m)
        <tr>
            <td>{{$m->kode_matakuliah}}</td>
            <td>{{$m->nama_matakuliah}}</td>
            <td>{{$m->kode_prodi}}</td>
            <td>{{$m->jenis_matakuliah}}</td>
            <td>{{$m->bobot_matakuliah}} / {{$m->bobot_teori}} / {{$m->bobot_pratikum}}</td>
            <td>
                <a class="btn btn-info btn-sm" data-toggle="modal" data-kode-matakuliah="{{$m->kode_matakuliah}}" data-target="#modal-edit">
                    <i class="fas fa-pencil-alt">
                    </i>
                    Edit
                </a>
                <a class="btn btn-danger btn-sm" data-toggle="modal" data-kode-matakuliah="{{$m->kode_matakuliah}}" data-nama-matakuliah="{{$m->nama_matakuliah}}" data-target="#modal-hapus">
                    <i class="fas fa-trash">
                    </i>
                    Delete
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <th>Kode Matakuliah</th>
        <th>Nama Matakuliah</th>
        <th>Kode Prodi</th>
        <th>Jenis</th>
        <th>Bobot(m/t/p)</th>
        <th>Aksi</th>
    </tr>
    </tfoot>
</table>
<!-- Bootstrap 4 -->
<script src="/dist/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables -->
<script src="/dist/plugins/datatables/jquery.dataTables.js"></script>
<script src="/dist/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
    $(function () {
        $("#tb-matakuliah").DataTable();
    });
</script>
</body>
</html>
