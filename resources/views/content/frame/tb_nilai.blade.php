<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body>
    <table id="tb-nilai" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>Nim</th>
            <th>Nama</th>
            <th>Matakuliah</th>
            <th>Nilai Angka</th>
            <th>Nilai Huruf</th>
            <th>IPS</th>
            <th>Aksi</th>
        </tr>
        </thead>
        <tbody id="body">
        @if(is_array($nilai) )
                    @foreach($nilai as $n)

                        <tr>
                            <td>{{$n->nim}}</td>
                            <td>{{$n->nama}}</td>
                            <td>{{$n->nama_matakuliah}}</td>
                            <td>{{$n->nilai_angka}}</td>
                            <td>{{$n->nilai_huruf}}</td>
                            <td>{{$n->ips}}</td>
                            <td>
                                <a class="btn btn-info btn-sm" data-toggle="modal" data-id-history-nilai="{{$n->id_history_nilai}}"
                                   data-target="#modal-edit">
                                    <i class="fas fa-pencil-alt">
                                    </i>
                                    Edit
                                </a>
                                <a class="btn btn-danger btn-sm" data-toggle="modal" data-id-history-nilai="{{$n->id_history_nilai}}"
                                   data-nama="{{$n->nama}}" data-target="#modal-hapus">
                                    <i class="fas fa-trash">
                                    </i>
                                    Delete
                                </a>
                            </td>
                        </tr>
                    @endforeach
        @elseif(!is_null($nilai))
            <tr>
                <td>{{$nilai->nim}}</td>
                <td>{{$nilai->nama}}</td>
                <td>{{$nilai->nama_matakuliah}}</td>
                <td>{{$nilai->nilai_angka}}</td>
                <td>{{$nilai->nilai_huruf}}</td>
                <td>{{$nilai->ips}}</td>
                <td>
                    <a class="btn btn-info btn-sm" data-toggle="modal" data-id-history-nilai="{{$n->id_history_nilai}}"
                       data-target="#modal-edit">
                        <i class="fas fa-pencil-alt">
                        </i>
                        Edit
                    </a>
                    <a class="btn btn-danger btn-sm" data-toggle="modal" data-id-history-nilai="{{$n->id_history_nilai}}"
                       data-nama="{{$n->nama}}" data-target="#modal-hapus">
                        <i class="fas fa-trash">
                        </i>
                        Delete
                    </a>
                </td>
            </tr>
        @else

        @endif

        </tbody>
        <tfoot>
        <tr>
            <th>Nim</th>
            <th>Nama</th>
            <th>Matakuliah</th>
            <th>Nilai Angka</th>
            <th>Nilai Huruf</th>
            <th>IPS</th>
            <th>Aksi</th>
        </tr>
        </tfoot>
    <!-- Bootstrap 4 -->
    <script src="/dist/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- DataTables -->
    <script src="/dist/plugins/datatables/jquery.dataTables.js"></script>
    <script src="/dist/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
    <script>
        $(function () {
            $("#tb-nilai").DataTable();
        });
    </script>
</body>
</html>
