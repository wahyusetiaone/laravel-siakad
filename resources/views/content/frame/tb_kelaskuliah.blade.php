<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body>
    <table id="tb-kelaskuliah" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>Kode Matakuliah</th>
            <th>Nama Matakuliah</th>
            <th>Nama Kelas</th>
            <th>Dosen</th>
            <th>Jumlah Peserta</th>
            <th>Aksi</th>
        </tr>
        </thead>
        <tbody id="body">
        @if(is_array($kk) )
                    @foreach($kk as $k)

                        <tr>
                            <td>{{$k->kode_matakuliah}}</td>
                            <td>{{$k->nama_matakuliah}}</td>
                            <td>{{$k->nama_kelas}}</td>
                            <td>Dummy Dosen</td>
                            <td>Dummy Pesertas</td>
                            <td>
                                <a class="btn btn-info btn-sm" data-toggle="modal" data-id-kelas-kuliah="{{$k->id_kelaskuliah}}"
                                   data-target="#modal-edit">
                                    <i class="fas fa-pencil-alt">
                                    </i>
                                    Edit
                                </a>
                                <a class="btn btn-danger btn-sm" data-toggle="modal" data-id-kelas-kuliah="{{$k->id_kelaskuliah}}"
                                   data-nama-matakuliah="{{$k->nama_matakuliah}}" data-target="#modal-hapus">
                                    <i class="fas fa-trash">
                                    </i>
                                    Delete
                                </a>
                            </td>
                        </tr>
                    @endforeach
        @elseif(!is_null($kk))
            <tr>
                <td>{{$kk->kode_matakuliah}}</td>
                <td>{{$kk->nama_matakuliah}}</td>
                <td>{{$kk->nama_kelas}}</td>
                <td>Dummy Dosen</td>
                <td>Dummy Pesertas</td>
                <td>
                    <a class="btn btn-info btn-sm" data-toggle="modal" data-id-kelas-kuliah="{{$kk->id_kelaskuliah}}"
                       data-target="#modal-edit">
                        <i class="fas fa-pencil-alt">
                        </i>
                        Edit
                    </a>
                    <a class="btn btn-danger btn-sm" data-toggle="modal" data-id-kelas-kuliah="{{$kk->id_kelaskuliah}}"
                       data-nama-matakuliah="{{$kk->nama_matakuliah}}" data-target="#modal-hapus">
                        <i class="fas fa-trash">
                        </i>
                        Delete
                    </a>
                </td>
            </tr>
        @else

        @endif

        </tbody>
        <tfoot>
        <tr>
            <th>Kode Matakuliah</th>
            <th>Nama Matakuliah</th>
            <th>Nama Kelas</th>
            <th>Dosen</th>
            <th>Jumlah Peserta</th>
            <th>Aksi</th>
        </tr>
        </tfoot>
    </table>
    <!-- Bootstrap 4 -->
    <script src="/dist/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- DataTables -->
    <script src="/dist/plugins/datatables/jquery.dataTables.js"></script>
    <script src="/dist/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
    <script>
        $(function () {
            $("#tb-kelaskuliah").DataTable();
        });
    </script>
</body>
</html>
