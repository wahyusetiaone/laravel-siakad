@extends('layouts.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Profile</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">User Profile</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3">
                        <!-- Profile Image -->
                        <div class="card card-primary card-outline" id="card-pendidikan">
                            <div class="card-body box-profile">
                                <div class="text-center">
                                    <img class="profile-user-img img-fluid img-circle"
                                         src="../../dist/img/user4-128x128.jpg"
                                         alt="User profile picture">
                                </div>

                                <h3 class="profile-username text-center">{{$mhs[1]}}</h3>

                                <p class="text-muted text-center">nik : {{$mhs[0]}}</p>

                                <ul class="list-group list-group-unbordered mb-3">
                                    <li class="list-group-item">
                                        <b>NISN</b> <a class="float-right">{{$mhs[7]}}</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>NPWP</b> <a class="float-right">{{$mhs[8]}}</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Jenis Kelamin</b> <a
                                            class="float-right">{{ $mhs[4] === "L" ? "Laki - Laki" : "Perempuan" }}</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Agama</b> <a class="float-right">{{ $mhs[5] }}</a>
                                    </li>
                                </ul>

                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->

                        <!-- About Me Box -->

                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-9">
                        <div class="card">
                            <div class="card-header p-2">
                                <ul class="nav nav-pills">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#pendidikan"data-toggle="tab">History Pendidikan</a>
                                    </li>
                                    <li class="nav-item"><a class="nav-link" href="#nilai" data-toggle="tab">Histori
                                            Nilai</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#transkip" data-toggle="tab">Transkip
                                            Nilai</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#profile"
                                                            data-toggle="tab">Profile</a></li>
                                </ul>
                            </div><!-- /.card-header -->
                            <div class="card-body">
                                <div class="tab-content">

                                    <div class="active tab-pane" id="pendidikan">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h3 class="card-title">
                                                            <a class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal-tambah">
                                                                <i class="fas fa-pencil-alt">
                                                                </i>
                                                                Tambah Pendidikan
                                                            </a>
                                                        </h3>
                                                        <div class="card-tools">
                                                            <button type="button" class="btn btn-tool" data-card-widget="card-refresh" data-source="/tabs/pendidikan/{{$mhs[0]}}" ><i class="fas fa-sync-alt"></i></button>
                                                            <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                                                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                                        </div>
                                                    </div>
                                                    <!-- /.card-header -->
                                                    <div class="card-body">

                                                        <!-- /.card-body -->
                                                    </div>
                                                    <!-- /.card -->
                                                </div>
                                                <!-- /.col -->
                                            </div>
                                            <!-- /.row -->
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="nilai">
                                        @if(!empty($nilai))
                                            @if(is_array($nilai))
                                                @foreach($nilai as $n)
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="card">
                                                                <div class="card-header">
                                                                    <h3 class="card-title">
                                                                        Semester {{$n->semester}}
                                                                    </h3>
                                                                    <div class="card-tools">
                                                                        <button type="button" class="btn btn-tool" data-card-widget="card-refresh" data-source="/tabs/nilai/{{$n->nim}}/{{$n->semester}}" ><i class="fas fa-sync-alt"></i></button>
                                                                        <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                                                                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                                                    </div>
                                                                </div>
                                                                <!-- /.card-header -->
                                                                <div class="card-body">

                                                                    <!-- /.card-body -->
                                                                </div>
                                                                <!-- /.card -->
                                                            </div>
                                                            <!-- /.col -->
                                                        </div>
                                                        <!-- /.row -->
                                                    </div>
                                                @endforeach
                                            @else
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="card">
                                                                <div class="card-header">
                                                                    <h3 class="card-title">
                                                                        Semester {{$nilai->semester}}
                                                                    </h3>
                                                                    <div class="card-tools">
                                                                        <button type="button" class="btn btn-tool" data-card-widget="card-refresh" data-source="/tabs/nilai/{{$nilai->nim}}/{{$nilai->semester}}" ><i class="fas fa-sync-alt"></i></button>
                                                                        <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                                                                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                                                    </div>
                                                                </div>
                                                                <!-- /.card-header -->
                                                                <div class="card-body">

                                                                    <!-- /.card-body -->
                                                                </div>
                                                                <!-- /.card -->
                                                            </div>
                                                            <!-- /.col -->
                                                        </div>
                                                        <!-- /.row -->
                                                    </div>
                                            @endif
                                        @else
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="card">
                                                        <div class="card-body">
                                                            No Record Found
                                                        </div>
                                                        <!-- /.card -->
                                                    </div>
                                                    <!-- /.col -->
                                                </div>
                                                <!-- /.row -->
                                            </div>
                                        @endif
                                    </div>

                                    <div class="tab-pane" id="transkip">
                                        @if(!empty($nilai))
                                            @if(is_array($nilai))
                                                @foreach($nilai as $n)
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="card">
                                                                <div class="card-header">
                                                                    <h3 class="card-title">
                                                                        Semester {{$n->semester}}
                                                                    </h3>
                                                                    <div class="card-tools">
                                                                        <button type="button" class="btn btn-tool" data-card-widget="card-refresh" data-source="/tabs/transkip/{{$n->nim}}/{{$n->semester}}" ><i class="fas fa-sync-alt"></i></button>
                                                                        <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                                                                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                                                    </div>
                                                                </div>
                                                                <!-- /.card-header -->
                                                                <div class="card-body">

                                                                    <!-- /.card-body -->
                                                                </div>
                                                                <div class="card-footer">
                                                                    <a class="btn btn-secondary btn-sm">
                                                                        <i class="fas fa-file-pdf">
                                                                        </i>
                                                                        Export PDF
                                                                    </a>
                                                                    <!-- /.card-footer -->
                                                                </div>
                                                                <!-- /.card -->
                                                            </div>
                                                            <!-- /.col -->
                                                        </div>
                                                        <!-- /.row -->
                                                    </div>
                                                @endforeach
                                            @else
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="card">
                                                            <div class="card-header">
                                                                <h3 class="card-title">
                                                                    Semester {{$nilai->semester}}
                                                                </h3>
                                                                <div class="card-tools">
                                                                    <button type="button" class="btn btn-tool" data-card-widget="card-refresh" data-source="/tabs/transkip/{{$nilai->nim}}/{{$nilai->semester}}" ><i class="fas fa-sync-alt"></i></button>
                                                                    <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                                                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                                                </div>
                                                            </div>
                                                            <!-- /.card-header -->
                                                            <div class="card-body">

                                                                <!-- /.card-body -->
                                                            </div>
                                                            <div class="card-footer">
                                                                <a class="btn btn-secondary btn-sm">
                                                                    <i class="fas fa-file-pdf">
                                                                    </i>
                                                                    Export PDF
                                                                </a>
                                                                <!-- /.card-footer -->
                                                            </div>
                                                            <!-- /.card -->
                                                        </div>
                                                        <!-- /.col -->
                                                    </div>
                                                    <!-- /.row -->
                                                </div>
                                            @endif
                                        @else
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="card">
                                                        <div class="card-body">
                                                            No Record Found
                                                        </div>
                                                        <!-- /.card -->
                                                    </div>
                                                    <!-- /.col -->
                                                </div>
                                                <!-- /.row -->
                                            </div>
                                        @endif
                                    </div>
                                    <div class="tab-pane" id="profile">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h3 class="card-title">
                                                            Profile
                                                        </h3>
                                                        <div class="card-tools">
                                                            <button type="button" class="btn btn-tool" data-card-widget="card-refresh" data-source="/tabs/profil/{{$mhs[0]}} ><i class="fas fa-sync-alt"></i></button>
                                                            <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                                                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                                        </div>
                                                    </div>
                                                    <!-- /.card-header -->
                                                    <div class="card-body">

                                                        <!-- /.card-body -->
                                                    </div>
                                                    <div class="card-footer">
                                                        <button type="submit" id="btn-edit-mhs" class="btn btn-info">Edit Profile</button>
                                                    </div>
                                                    <!-- /.card -->
                                                </div>
                                                <!-- /.col -->
                                            </div>
                                            <!-- /.row -->
                                        </div>
                                    </div>
                                    <!-- /.tab-pane -->
                                </div>
                                <!-- /.tab-content -->
                            </div><!-- /.card-body -->
                        </div>
                        <!-- /.nav-tabs-custom -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Modal Start -->
    <div class="modal fade" id="modal-tambah">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Pendidikan</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" id="form-tambah-pendidikan">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <!-- form start -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="nim">NIM</label>
                                        <input type="number" class="form-control" id="nim" name="nim" placeholder="NIM">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="nik">NIK</label>
                                        <input type="number" class="form-control" id="nik" name="nik" placeholder="NIK" value="{{$mhs[0]}}" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="jenis_pendaftaran">Jenis Pendaftaran</label>
                                        <select class="form-control" id="jenis_pendaftaran" name="jenis_pendaftaran">
                                            <option value="baru" >Baru</option>
                                            <option value="pindahan" >Pindah</option>
                                            <option value="transfer" >Transfer</option>
                                            <option value="alih jenjang" >Alih Jenjang</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="jalur_pendaftaran">Jalur Pendaftaran</label>
                                        <select class="form-control" id="jalur_pendaftaran" name="jalur_pendaftaran">
                                            <option value="mandiri" >Mandiri</option>
                                            <option value="beasiswa" >Beasiswa</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="tanggal_masuk"> Tanggal Masuk </label>
                                        <input type="date" class="form-control" id="tanggal_masuk" name="tanggal_masuk" placeholder="Tanggal Masuk">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="perguruan_tinggi"> Perguruan Tinggi </label>
                                        <input type="text" class="form-control" id="perguruan_tinggi" name="perguruan_tinggi" placeholder="Perguruan Tinggi">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="kode_prodi">Kode Prodi</label>
                                        <select class="form-control" id="kode_prodi" name="kode_prodi">
                                            @foreach($allprodi as $ap)
                                                <option value="{{$ap->kode_prodi}}" >{{$ap->nama_prodi}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Tambah Pendidikan</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- Modal Start -->
    <div class="modal fade" id="modal-edit">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Pendidikan</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" id="form-edit-pendidikan">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <!-- form start -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="nim">NIM</label>
                                        <input type="number" class="form-control" id="e_nim" name="nim" placeholder="NIM">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="nik">NIK</label>
                                        <input type="number" class="form-control" id="e_nik" name="nik" placeholder="NIK" value="{{$mhs[0]}}" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="jenis_pendaftaran">Jenis Pendaftaran</label>
                                        <select class="form-control" id="e_jenis_pendaftaran" name="jenis_pendaftaran">
                                            <option value="baru" >Baru</option>
                                            <option value="pindahan" >Pindah</option>
                                            <option value="transfer" >Transfer</option>
                                            <option value="alih jenjang" >Alih Jenjang</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="jalur_pendaftaran">Jalur Pendaftaran</label>
                                        <select class="form-control" id="e_jalur_pendaftaran" name="jalur_pendaftaran">
                                            <option value="mandiri" >Mandiri</option>
                                            <option value="beasiswa" >Beasiswa</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="tanggal_masuk"> Tanggal Masuk </label>
                                        <input type="date" class="form-control" id="e_tanggal_masuk" name="tanggal_masuk" placeholder="Tanggal Masuk">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="perguruan_tinggi"> Perguruan Tinggi </label>
                                        <input type="text" class="form-control" id="e_perguruan_tinggi" name="perguruan_tinggi" placeholder="Perguruan Tinggi">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="kode_prodi">Kode Prodi</label>
                                        <select class="form-control" id="e_kode_prodi" name="kode_prodi">
                                            @foreach($allprodi as $ap)
                                                <option value="{{$ap->kode_prodi}}" >{{$ap->nama_prodi}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Edit Pendidikan</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div class="modal fade" id="modal-hapus">
        <div class="modal-dialog modal-md">
            <form role="form" id="form-hapus-pendidikan">
                {{ csrf_field() }}
                <div class="modal-content">
                    <for class="modal-body">
                        <div class="modal-header">
                            <h4 class="modal-title">Hapus Pendidikan</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Yakin ingin menghapus data pendidikan dengan kode prodi <span id="h_span_kode_prodi"></span> dengan nama <span id="h_span_nama_prodi"></span> ? Menghapus data pendidikan juga akan menghapus semua history nilai yang terkait !</p>
                            <input type="hidden" id="h_nim" name="nim">
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Ya, Yakin</button>
                        </div>
                </div>
            </form>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

@endsection

@section('javascript')
    <!-- jQuery -->
    <script src="/dist/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="/dist/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="/dist/js/demo.js"></script>
    <!-- SweetAlert2 -->
    <script src="/dist/plugins/sweetalert2/sweetalert2.min.js"></script>
    <!-- Toastr -->
    <script src="/dist/plugins/toastr/toastr.min.js"></script>
    <script>
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
        });

        $('.card-tools [data-card-widget="card-refresh"]').click();

        $('#btn-edit-mhs').click(function (e) {
            e.preventDefault();
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url:'/mahasiswa/edit',
                type:'post',
                data:$('#form-edit-mhs').serialize(),
                success:function(){
                    Toast.fire({
                        type: 'success',
                        title: 'Berhasil edit Mahasiswa !!!'
                    });
                    $('.card-tools [data-card-widget="card-refresh"]').click();
                }
            });
        })

        $('#form-tambah-pendidikan').submit(function(e){
            e.preventDefault();
            $.ajax({
                url:'/pendidikan/tambah',
                type:'post',
                data:$('#form-tambah-pendidikan').serialize(),
                success:function(data){
                    console.log(data);
                    $('#modal-tambah').modal('hide');
                    Toast.fire({
                        type: 'success',
                        title: 'Pendidikan Berhasil ditambahkan !!!'
                    });
                    $('.card-tools [data-card-widget="card-refresh"]').click();
                }
            });
        });

        //triggered when modal is about to be shown
        $('#modal-edit').on('show.bs.modal', function(e) {

            //get data-id attribute of the clicked element
            var nim = $(e.relatedTarget).data('nim');
            $.ajax({
                url:'/pendidikan/get',
                type:'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data:{
                    'nim' : nim
                },
                dataType: 'json',
                success: function (response) {
                    var pr = response.data.return;
                    $('#e_nim').val(pr[1]);
                    $('#e_nik').val(pr[0]);
                    $('#e_nama').val(pr[2]);
                    $('#e_jenis_pendaftaran option[value="'+pr[3]+'"]').prop('selected', true);
                    $('#e_jalur_pendaftaran option[value="'+pr[4]+'"]').prop('selected', true);
                    $('#e_tanggal_masuk').val(pr[5]);
                    $('#e_perguruan_tinggi').val(pr[6]);
                    $('#e_kode_prodi option[value="'+pr[7]+'"]').prop('selected', true);
                },
            });
        }).submit(function (e) {
            e.preventDefault();
            $.ajax({
                url:'/pendidikan/edit',
                type:'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data:$('#form-edit-pendidikan').serialize(),
                success:function(){
                    $('#modal-edit').modal('hide');
                    Toast.fire({
                        type: 'success',
                        title: 'Berhasil edit Pendidikan !!!'
                    });
                    $('.card-tools [data-card-widget="card-refresh"]').click();
                }
            });
        })

        $('#modal-hapus').on('show.bs.modal', function(e) {
            var kode_prodi = $(e.relatedTarget).data('kode-prodi');
            var nama_prodi = $(e.relatedTarget).data('nama-prodi');
            var nim = $(e.relatedTarget).data('nim');
            $('#h_span_kode_prodi').text(kode_prodi);
            $('#h_span_nama_prodi').text(nama_prodi);
            $('#h_nim').val(nim);
        }).submit(function(e){
            e.preventDefault();
            $.ajax({
                url:'/pendidikan/hapus',
                type:'post',
                data:$('#form-hapus-pendidikan').serialize(),
                success:function(){
                    $.ajax({
                        url:'/nilai/hapus-use-nim',
                        type:'post',
                        data:$('#form-hapus-pendidikan').serialize(),
                        success:function(){
                            $('#modal-hapus').modal('hide');
                            Toast.fire({
                                type: 'success',
                                title: 'Data Pendidikan Berhasil dihapus !!!'
                            });
                            $('.card-tools [data-card-widget="card-refresh"]').click();
                        }
                    });
                }
            });
        });

    </script>
@stop
