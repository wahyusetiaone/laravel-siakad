@extends('layouts.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Nilai</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Nilai</a></li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <h3 class="card-title">
                                    <a class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal-tambah">
                                        <i class="fas fa-pencil-alt">
                                        </i>
                                        Tambah Nilai
                                    </a>
                                </h3>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="jenis_matakuliah">Program Studi</label>
                                        <select class="form-control" id="s_kode_prodi" name="kode_prodi">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="jenis_matakuliah">Semester</label>
                                        <select class="form-control" id="s_semester" name="semester">
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div id="tb">
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Modal Start -->
    <div class="modal fade" id="modal-tambah">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Nilai Kelas</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" id="form-tambah">
{{--                    {{ csrf_field() }}--}}
                    <div class="modal-body">
                        <!-- form start -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="id_history_nilai">ID Nilai</label>
                                        <input type="text" class="form-control" id="t_id_history_nilai" name="id_history_nilai" placeholder="ID Nilai">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="nim">Mahasiswa</label>
                                        <select class="form-control" id="t_nim" name="nim">
                                            @foreach($mhs as $m)
                                                <option value="{{$m->nim}}" >( {{$m->nim}} ) {{$m->nama}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="periode">Periode</label>
                                        <input type="text" class="form-control" id="t_periode" name="periode" placeholder="Periode">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="id_kelas_kuliah">Kelas Kuliah</label>
                                        <select class="form-control" id="t_id_kelas_kuliah" name="id_kelas_kuliah">
                                            @foreach($kk as $k)
                                                <option value="{{$k->id_kelaskuliah}}" >{{$k->nama_kelas}} | {{$k->semester}} | {{$k->nama_matakuliah}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="nilai_angka">Nilai Angka</label>
                                        <input type="number" class="form-control" id="t_nilai_angka" name="nilai_angka" placeholder="Nilai Angka">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="nilai_huruf">Nilai Huruf</label>
                                        <input type="text" class="form-control" id="t_nilai_huruf" name="nilai_huruf" placeholder="Nilai Huruf" readonly="true">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="nilai_indek">Nilai Indek</label>
                                        <input type="text" class="form-control" id="t_nilai_indek" name="nilai_indek" placeholder="Nilai Indek" readonly="true">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="ips">IPS</label>
                                        <input type="text" class="form-control" id="t_ips" name="ips" placeholder="IPS" readonly="true">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Tambah Nilai</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- Modal Start -->
    <div class="modal fade" id="modal-edit">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Nilai</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" id="form-edit">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <!-- form start -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="id_history_nilai">ID Nilai</label>
                                        <input type="text" class="form-control" id="e_id_history_nilai" name="id_history_nilai" placeholder="ID Nilai" readonly="true">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="nim">Mahasiswa</label>
                                        <select class="form-control" id="e_nim" name="nim">
                                            @foreach($mhs as $m)
                                                <option value="{{$m->nim}}" >( {{$m->nim}} ) {{$m->nama}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="periode">Periode</label>
                                        <input type="text" class="form-control" id="e_periode" name="periode" placeholder="Periode">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="id_kelas_kuliah">Kelas Kuliah</label>
                                        <select class="form-control" id="e_id_kelas_kuliah" name="id_kelas_kuliah">
                                            @foreach($kk as $k)
                                                <option value="{{$k->id_kelaskuliah}}" >{{$k->nama_kelas}} | {{$k->semester}} | {{$k->nama_matakuliah}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="nilai_angka">Nilai Angka</label>
                                        <input type="number" class="form-control" id="e_nilai_angka" name="nilai_angka" placeholder="Nilai Angka">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="nilai_huruf">Nilai Huruf</label>
                                        <input type="text" class="form-control" id="e_nilai_huruf" name="nilai_huruf" placeholder="Nilai Huruf" readonly="true">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="nilai_indek">Nilai Indek</label>
                                        <input type="text" class="form-control" id="e_nilai_indek" name="nilai_indek" placeholder="Nilai Indek" readonly="true">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="ips">IPS</label>
                                        <input type="text" class="form-control" id="e_ips" name="ips" placeholder="IPS" readonly="true">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Edit Nilai</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div class="modal fade" id="modal-hapus">
        <div class="modal-dialog modal-md">
            <form role="form" id="form-hapus">
{{--                {{ csrf_field() }}--}}
                <div class="modal-content">
                    <for class="modal-body">
                        <div class="modal-header">
                            <h4 class="modal-title">Hapus Nilai</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Yakin ingin menghapus Kode <span id="h_span_id_history_nilai"></span> dengan nama <span id="h_span_nama"></span></p>
                            <input type="hidden" id="h_id_history_nilai" name="id_history_nilai">
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Ya, Yakin</button>
                        </div>
                </div>
            </form>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

@endsection

@section('javascript')
    <!-- jQuery -->
    <script src="/dist/plugins/jquery/jquery.min.js"></script>
    <!-- DataTables -->
    <script src="/dist/plugins/datatables/jquery.dataTables.js"></script>
    <script src="/dist/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
    <!-- SweetAlert2 -->
    <script src="/dist/plugins/sweetalert2/sweetalert2.min.js"></script>
    <!-- Toastr -->
    <script src="/dist/plugins/toastr/toastr.min.js"></script>
    <!-- AdminLTE App -->
    <script src="/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="/dist/js/demo.js"></script>
    <!-- page script -->
    <script>

        var newProdi = {"All":"all"};
        var newSemester = {"All":"all"};

        $( document ).ready(function() {
            fistTimeOptions();
        });

        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
        });

        //triggered when modal is about to be shown
        $('#modal-edit').on('show.bs.modal', function(e) {

            //get data-id attribute of the clicked element
            var id_history_nilai = $(e.relatedTarget).data('id-history-nilai');
            $.ajax({
                url:'nilai/get',
                type:'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data:{
                    'id_history_nilai' : id_history_nilai
                },
                dataType: 'json',
                success: function (response) {
                    var n = response.data.return;
                    $('#e_id_history_nilai').val(n[0]);
                    $('#e_nim option[value="'+n[1]+'"]').prop('selected', true);
                    $('#e_periode').val(n[2]);
                    $('#e_id_kelas_kuliah option[value="'+n[3]+'"]').prop('selected', true);
                    $('#e_nilai_angka').val(n[4]);
                    $('#e_nilai_huruf').val(n[5]);
                    $('#e_nilai_indek').val(n[6]);
                    $('#e_ips').val(n[7]);
                },
            });
        }).submit(function (e) {
            e.preventDefault();
            $.ajax({
                url:'nilai/edit',
                type:'post',
                data:$('#form-edit').serialize(),
                success:function(){
                    $('#modal-edit').modal('hide');
                    //for backdrop not remove
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    Toast.fire({
                        type: 'success',
                        title: 'Berhasil edit Program Studi !!!'
                    });
                    resetFactoryTable();
                }
            });
        })

        $('#form-tambah').submit(function(e){
            e.preventDefault();
            $.ajax({
                url:'nilai/tambah',
                type:'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data:$('#form-tambah').serialize(),
                success:function(){
                    $('#modal-tambah').modal('hide');
                    //for backdrop not remove
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    Toast.fire({
                        type: 'success',
                        title: 'Program Studi Berhasil ditambahkan !!!'
                    });
                    resetFactoryTable();
                }
            });
        });

        $('#modal-hapus').on('show.bs.modal', function(e) {
            var id_history_nilai = $(e.relatedTarget).data('id-history-nilai');
            var nama = $(e.relatedTarget).data('nama');
            $('#h_span_id_history_nilai').text(id_history_nilai);
            $('#h_span_nama').text(nama);
            $('#h_id_history_nilai').val(id_history_nilai);
        }).submit(function(e){
            e.preventDefault();
            $.ajax({
                url:'nilai/hapus',
                type:'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data:$('#form-hapus').serialize(),
                success:function(){
                    $('#modal-hapus').modal('hide');
                    //for backdrop not remove
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    Toast.fire({
                        type: 'success',
                        title: 'Program Studi Berhasil dihapus !!!'
                    });
                    resetFactoryTable();
                }
            });
        });

        function fistTimeOptions() {

            @foreach($nilai as $n)
                newProdi["{{$n->nama_prodi}}"] = "{{$n->kode_prodi}}";
                newSemester["{{$n->semester}}"] = "{{$n->semester}}";
            @endforeach

            var $prodi = $("#s_kode_prodi");
            $prodi.empty(); // remove old options
            $.each(newProdi, function(key,value) {
                $prodi.append($("<option></option>")
                    .attr("value", value).text(key));
            });

            resetFactoryTable();

        }

        function resetFactoryTable(){
            $('#s_kode_prodi option[value="all"]').prop('selected', true);

            var $semester = $("#s_semester");
            $semester.empty(); // remove old options
            $.each(newSemester, function(key,value) {
                $semester.append($("<option></option>")
                    .attr("value", value).text(key));
            });
            $('#s_semester option[value="all"]').prop('selected', true);

            // $('#body').load("kelaskuliah/tampilQuery/sistem%20informatika/3");
            $('#tb').load("nilai/tampilQuery/all/all");
        }

        $('#s_kode_prodi').on('change', function() {
            var kode_prodi = $('#s_kode_prodi').val();
            var semester = $('#s_semester').val();
            $('#tb').load("nilai/tampilQuery/"+kode_prodi+"/"+semester);
        });

        $('#s_semester').on('change', function() {
            var kode_prodi = $('#s_kode_prodi').val();
            var semester = $('#s_semester').val();
            $('#tb').load("nilai/tampilQuery/"+kode_prodi+"/"+semester);
        });

        $('#t_nilai_angka').on('input', function () {
            calculating($(this).val(),'t_nilai_huruf','t_nilai_indek','t_ips');
        });

        $('#e_nilai_angka').on('input', function () {
            calculating($(this).val(),'e_nilai_huruf','e_nilai_indek','e_ips');
        });

        function calculating(nilai,huruf,index,ips) {

            if (nilai >= 85 && nilai <=100){
                $('#'+huruf).val('A');
                $('#'+index).val('4');
                $('#'+ips).val('4');
            }
            else if (nilai > 75 && nilai < 85){
                $('#'+huruf).val('B');
                $('#'+index).val('3');
                $('#'+ips).val('3');
            }
            else if (nilai > 65 && nilai < 75){
                $('#'+huruf).val('C');
                $('#'+index).val('2');
                $('#'+ips).val('2');
            }
            else if (nilai > 50 && nilai < 65){
                $('#'+huruf).val('D');
                $('#'+index).val('1');
                $('#'+ips).val('1');
            }
            else {
                $('#'+huruf).val('E');
                $('#'+index).val('0');
                $('#'+ips).val('0');
            }
        }

    </script>
@stop
