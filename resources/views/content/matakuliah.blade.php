@extends('layouts.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Matakuliah</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Matakuliah</a></li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <a class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal-tambah">
                                    <i class="fas fa-pencil-alt">
                                    </i>
                                    Tambah Matakuliah
                                </a>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
{{--                            tb--}}
                            <div id="tb"></div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Modal Start -->
    <div class="modal fade" id="modal-tambah">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Matakuliah</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" id="form-tambah">
                    {{ csrf_field() }}
                <div class="modal-body">
                    <!-- form start -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="kode_matakuliah">Kode Matakuliah</label>
                                        <input type="text" class="form-control" id="t_kode_matakuliah" name="kode_matakuliah" placeholder="Kode Matakuliah">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="nama_matakuliah">Nama Matakuliah</label>
                                        <input type="text" class="form-control" id="t_nama_matakuliah" name="nama_matakuliah" placeholder="Nama Matakuliah">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="kode_prodi">Kode Program Studi</label>
                                        <input type="text" class="form-control" id="t_kode_prodi" name="kode_prodi" placeholder="Kode Program Studi">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="jenis_matakuliah">Jenis Matakuliah</label>
                                        <select class="form-control" id="t_jenis_matakuliah" name="jenis_matakuliah">
                                            <option value="wajib" >Wajib</option>
                                            <option value="pilihan" >Pilihan</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="bobot_matakuliah"> Bobot Matakuliah </label>
                                        <input type="number" class="form-control" id="t_bobot_matakuliah" name="bobot_matakuliah" placeholder="Bobot Matakuliah">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="bobot_teori">Bobot Teori</label>
                                        <input type="number" class="form-control" id="t_bobot_teori" name="bobot_teori" placeholder="Bobot Teori">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="bobot_praktikum">Bobot Praktikum</label>
                                        <input type="number" class="form-control" id="t_bobot_praktikum" name="bobot_praktikum" placeholder="Bobot Praktikum">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Tambah Matakuliah</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- Modal Start -->
    <div class="modal fade" id="modal-edit">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Matakuliah</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" id="form-edit">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <!-- form start -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="kode_matakuliah">Kode Matakuliah</label>
                                        <input type="text" class="form-control" id="e_kode_matakuliah" name="kode_matakuliah" placeholder="Kode Matakuliah" readonly>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="nama_matakuliah">Nama Matakuliah</label>
                                        <input type="text" class="form-control" id="e_nama_matakuliah" name="nama_matakuliah" placeholder="Nama Matakuliah">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="kode_prodi">Kode Program Studi</label>
                                        <input type="text" class="form-control" id="e_kode_prodi" name="kode_prodi" placeholder="Kode Program Studi">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="jenis_matakuliah">Jenis Matakuliah</label>
                                        <select class="form-control" id="e_jenis_matakuliah" name="jenis_matakuliah">
                                            <option value="wajib" >Wajib</option>
                                            <option value="pilihan" >Pilihan</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="bobot_matakuliah"> Bobot Matakuliah </label>
                                        <input type="number" class="form-control" id="e_bobot_matakuliah" name="bobot_matakuliah" placeholder="Bobot Matakuliah">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="bobot_teori">Bobot Teori</label>
                                        <input type="number" class="form-control" id="e_bobot_teori" name="bobot_teori" placeholder="Bobot Teori">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="bobot_praktikum">Bobot Praktikum</label>
                                        <input type="number" class="form-control" id="e_bobot_praktikum" name="bobot_praktikum" placeholder="Bobot Praktikum">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Edit Matakuliah</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div class="modal fade" id="modal-hapus">
        <div class="modal-dialog modal-md">
            <form role="form" id="form-hapus">
                {{ csrf_field() }}
            <div class="modal-content">
                    <for class="modal-body">
                <div class="modal-header">
                    <h4 class="modal-title">Hapus Matakuliah</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Yakin ingin menghapus Kode <span id="h_span_kode_matakuliah"></span> dengan nama <span id="h_span_nama_matakuliah"></span></p>
                    <input type="hidden" id="h_kode_matakuliah" name="kode_matakuliah">
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Ya, Yakin</button>
                </div>
            </div>
            </form>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

@endsection

@section('javascript')
    <!-- jQuery -->
    <script src="/dist/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="/dist/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- DataTables -->
    <script src="/dist/plugins/datatables/jquery.dataTables.js"></script>
    <script src="/dist/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
    <!-- SweetAlert2 -->
    <script src="/dist/plugins/sweetalert2/sweetalert2.min.js"></script>
    <!-- Toastr -->
    <script src="/dist/plugins/toastr/toastr.min.js"></script>
    <!-- AdminLTE App -->
    <script src="/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="/dist/js/demo.js"></script>
    <!-- page script -->
    <script>
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
        });

        $( document ).ready(function() {
            fn_loadtb();
        });

        // $(function () {
        //     $('#tb-matakuliah').DataTable({
        //         "paging": true,
        //         "lengthChange": true,
        //         "searching": true,
        //         "ordering": true,
        //         "info": true,
        //         "autoWidth": true
        //     });
        // });

        //triggered when modal is about to be shown
        $('#modal-edit').on('show.bs.modal', function(e) {

            //get data-id attribute of the clicked element
            var kode_matakuliah = $(e.relatedTarget).data('kode-matakuliah');
            $.ajax({
                url:'matakuliah/get',
                type:'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data:{
                    'kode_matakuliah' : kode_matakuliah
                },
                dataType: 'json',
                success: function (response) {
                    var mtk = response.data.return;
                    $('#e_kode_matakuliah').val(mtk[0]);
                    $('#e_nama_matakuliah').val(mtk[1]);
                    $('#e_kode_prodi').val(mtk[2]);
                    $('#e_jenis_matakuliah option[value="'+mtk[3]+'"]').prop('selected', true);
                    $('#e_bobot_matakuliah').val(mtk[5]);
                    $('#e_bobot_teori').val(mtk[6]);
                    $('#e_bobot_praktikum').val(mtk[7]);
                },
            });
        }).submit(function (e) {
            e.preventDefault();
            console.log($('#form-edit').serialize());
            $.ajax({
                url:'matakuliah/edit',
                type:'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data:$('#form-edit').serialize(),
                success:function(){
                    $('#modal-edit').modal('hide');
                    Toast.fire({
                        type: 'success',
                        title: 'Berhasil edit matakuliah !!!'
                    });
                    fn_loadtb();
                }
            });
        })

        $('#form-tambah').submit(function(e){
            e.preventDefault();
            $.ajax({
                url:'matakuliah/tambah',
                type:'post',
                data:$('#form-tambah').serialize(),
                success:function(){
                    $('#modal-tambah').modal('hide');
                    Toast.fire({
                        type: 'success',
                        title: 'Matakuliah Berhasil ditambahkan !!!'
                    });
                    fn_loadtb();
                }
            });
        });

        $('#modal-hapus').on('show.bs.modal', function(e) {
            var kode_matakuliah = $(e.relatedTarget).data('kode-matakuliah');
            var nama_matakuliah = $(e.relatedTarget).data('nama-matakuliah');
            $('#h_span_kode_matakuliah').text(kode_matakuliah);
            $('#h_span_nama_matakuliah').text(nama_matakuliah);
            $('#h_kode_matakuliah').val(kode_matakuliah);
        }).submit(function(e){
            e.preventDefault();
            $.ajax({
                url:'matakuliah/hapus',
                type:'post',
                data:$('#form-hapus').serialize(),
                success:function(){
                    $('#modal-hapus').modal('hide');
                    Toast.fire({
                        type: 'success',
                        title: 'Matakuliah Berhasil dihapus !!!'
                    });
                    fn_loadtb();
                }
            });
        });

        function fn_loadtb() {
            $( "#tb" ).load( "matakuliah/tb" );
        }
    </script>
@stop
