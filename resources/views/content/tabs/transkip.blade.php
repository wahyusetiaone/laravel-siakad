<table class="table table-bordered" id="tb-pendidikan">
    <thead>
    <tr>
        <th>Nama</th>
        <th>Nilai Angka</th>
        <th>Nilai Huruf</th>
    </tr>
    </thead>
    <tbody>
    @if(!empty($transkip))
        @if(is_array($transkip))
            @foreach($transkip as $n)
                <tr>
                    <td>{{$n->nama_matakuliah}}</td>
                    <td>{{$n->nilai_angka}}</td>
                    <td>{{$n->nilai_huruf}}</td>
                </tr>
            @endforeach
        @else
            <tr>
                <td>{{$transkip->nama_matakuliah}}</td>
                <td>{{$transkip->nilai_angka}}</td>
                <td>{{$transkip->nilai_huruf}}</td>
            </tr>
        @endif
    @else
        No Record Found
    @endif
    </tbody>
</table>
