<table class="table table-bordered" id="tb-pendidikan">
    <thead>
    <tr>
        <th style="width: 10px">#</th>
        <th>Nim</th>
        <th>Jenis Pendaftaran</th>
        <th>Tanggal Masuk</th>
        <th>Prodi</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    @php
        $x = 1
    @endphp
    @if(!empty($mhs))
        @if(is_array($mhs))
            @foreach($mhs as $m)
                <tr>
                    <td>{{$x}}</td>
                    <td>{{$m->nim}}</td>
                    <td>{{$m->jenis_pendaftaran}}</td>
                    <td>{{$m->tgl_masuk}}</td>
                    <td>{{$m->nama_prodi}}</td>
                    <td>
                        <a class="btn btn-info btn-sm" data-toggle="modal" data-nim="{{$m->nim}}" data-target="#modal-edit">
                            <i class="fas fa-pencil-alt">
                            </i>
                            Edit
                        </a>
                        <a class="btn btn-danger btn-sm" data-toggle="modal" data-nim="{{$m->nim}}" data-target="#modal-hapus">
                            <i class="fas fa-trash">
                            </i>
                            Delete
                        </a>
                    </td>
                </tr>
                @php
                    $x += 1
                @endphp
            @endforeach
        @else
            <tr>
                <td>{{$x}}</td>
                <td>{{$mhs->nim}}</td>
                <td>{{$mhs->jenis_pendaftaran}}</td>
                <td>{{$mhs->tgl_masuk}}</td>
                <td>{{$mhs->nama_prodi}}</td>
                <td>
                    <a class="btn btn-info btn-sm" data-toggle="modal" data-nim="{{$mhs->nim}}" data-target="#modal-edit">
                        <i class="fas fa-pencil-alt">
                        </i>
                        Edit
                    </a>
                    <a class="btn btn-danger btn-sm" data-toggle="modal" data-nim="{{$mhs->nim}}" data-kode-prodi="{{$mhs->kode_prodi}}" data-nama-prodi="{{$mhs->nama_prodi}}" data-target="#modal-hapus">
                        <i class="fas fa-trash">
                        </i>
                        Delete
                    </a>
                </td>
            </tr>
        @endif
    @else
        No Record Found
    @endif
    </tbody>
</table>
