<table class="table table-bordered" id="tb-pendidikan">
    <thead>
    <tr>
        <th>Nama</th>
        <th>Nilai Angka</th>
        <th>Nilai Huruf</th>
    </tr>
    </thead>
    <tbody>
    @if(!empty($nilai))
        @if(is_array($nilai))
            @foreach($nilai as $n)
                <tr>
                    <td>{{$n->nama_matakuliah}}</td>
                    <td>{{$n->nilai_angka}}</td>
                    <td>{{$n->nilai_huruf}}</td>
                </tr>
            @endforeach
        @else
            <tr>
                <td>{{$nilai->nama_matakuliah}}</td>
                <td>{{$nilai->nilai_angka}}</td>
                <td>{{$nilai->nilai_huruf}}</td>
            </tr>
        @endif
    @else
        No Record Found
    @endif
    </tbody>
</table>
