<form id="form-edit-mhs">
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="nik">NIK</label>
                <input type="text" class="form-control" id="nik" name="nik" placeholder="NIK" value="{{$dataM[0]}}" readonly>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="nisn">NISN</label>
                <input type="text" class="form-control" id="nisn" name="nisn" placeholder="NISN" value="{{$dataM[7]}}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="npwp">NPWP</label>
                <input type="text" class="form-control" id="npwp" name="npwp" placeholder="NPWP" value="{{$dataM[8]}}">
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="nama">Nama Mahasiswa</label>
                <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Mahasiswa" value="{{$dataM[1]}}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="tanggal_lahir">Tanggal Lahir</label>
                <input type="date" class="form-control" id="tanggal_lahir" name="tanggal_lahir" placeholder="Tanggal Lahir" value="{{
                    date_format (new DateTime($dataM[3]), 'Y-m-d')
                }}">
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="tempat_lahir">Tempat Lahir</label>
                <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" placeholder="Tempat Lahir" value="{{$dataM[2]}}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="jenis_kelamin">Jenis Kelamin</label>
                <select class="form-control" id="jenis_kelamin" name="jenis_kelamin">
                    <option value="L" @if ($dataM[4] === 'L')
                        {{'selected'}}
                    @endif>Laki - laki</option>
                    <option value="P" @if ($dataM[4] === 'P')
                        {{'selected'}}
                        @endif>Perempuan</option>
                </select>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="agama">Agama</label>
                <input type="text" class="form-control" id="agama" name="agama" placeholder="Agama" value="{{$dataM[5]}}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="nama_ibu_kandung">Nama Ibu Kandung</label>
                <input type="text" class="form-control" id="nama_ibu_kandung" name="nama_ibu_kandung" placeholder="Nama Ibu" value="{{$dataM[6]}}">
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="kewarganegaraan">Kewarganegaraan</label>
                <input type="text" class="form-control" id="kewarganegaraan" name="kewarganegaraan" placeholder="Kewarganegaraan" value="{{$dataM[9]}}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2">
            <div class="form-group">
                <label for="rt">RT</label>
                <input type="number" class="form-control" id="rt" name="rt" placeholder="RT" value="{{$dataM[12]}}">
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <label for="rw">RW</label>
                <input type="number" class="form-control" id="rw" name="rw" placeholder="RW" value="{{$dataM[13]}}">
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <label for="jalan">Jalan</label>
                <input type="text" class="form-control" id="jalan" name="jalan" placeholder="Jalan" value="{{$dataM[10]}}">
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <label for="dusun">Dusun</label>
                <input type="text" class="form-control" id="dusun" name="dusun" placeholder="Dusun" value="{{$dataM[11]}}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-5">
            <div class="form-group">
                <label for="kelurahan">Kelurahan</label>
                <input type="text" class="form-control" id="kelurahan" name="kelurahan" placeholder="Kelurahan" value="{{$dataM[14]}}">
            </div>
        </div>
        <div class="col-sm-5">
            <div class="form-group">
                <label for="kecamatan">Kecamatan</label>
                <input type="text" class="form-control" id="kecamatan" name="kecamatan" placeholder="Kecamatan" value="{{$dataM[15]}}">
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <label for="kode_pos">Kode Pos</label>
                <input type="text" class="form-control" id="kode_pos" name="kode_pos" placeholder="Kode Pos" value="{{$dataM[16]}}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <div class="form-group">
                <label for="telepon"> Telepon </label>
                <input type="text" class="form-control" id="telepon" name="telepon" placeholder="Telepon" value="{{$dataM[17]}}">
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <label for="hp">HP</label>
                <input type="text" class="form-control" id="hp" name="hp" placeholder="HP" value="{{$dataM[18]}}">
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{$dataM[19]}}">
            </div>
        </div>
    </div>
</form>
