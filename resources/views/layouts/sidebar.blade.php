<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
        <img src="/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">Siakad</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">Ajid Gaple</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->

                <li class="nav-item {!! classActivePath(1,'dashboard') !!}">
                    <a href="{!! route('dashboard') !!}" class="nav-link {!! classActiveSegment(1, 'dashboard') !!}">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{!! route('mahasiswa') !!}" class="nav-link {!! classActiveSegment(1, 'mahasiswa') !!}">
                        <i class="nav-icon far fa-calendar-alt"></i>
                        <p>
                            Mahasiswa
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{!! route('matakuliah') !!}" class="nav-link {!! classActiveSegment(1,'matakuliah') !!}">
                        <i class="nav-icon far fa-calendar-alt"></i>
                        <p>
                            Matakuliah
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{!! route('prodi') !!}" class="nav-link {!! classActiveSegment(1,'prodi') !!}">
                        <i class="nav-icon far fa-calendar-alt"></i>
                        <p>
                            Prodi
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{!! route('kelaskuliah') !!}" class="nav-link {!! classActiveSegment(1,'kelaskuliah') !!}">
                        <i class="nav-icon far fa-calendar-alt"></i>
                        <p>
                            Kelas Kuliah
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{!! route('nilai') !!}" class="nav-link {!! classActiveSegment(1,'nilai') !!}">
                        <i class="nav-icon far fa-calendar-alt"></i>
                        <p>
                            Nilai
                        </p>
                    </a>
                </li>
{{--                <li class="nav-item has-treeview {!! classActivePath(1 , 'kelas') !!}">--}}
{{--                    <a href="#" class="nav-link {!! classActiveSegment(1, 'kelas') !!}">--}}
{{--                        <i class="nav-icon fas fa-copy"></i>--}}
{{--                        <p>--}}
{{--                            Kelas--}}
{{--                            <i class="fas fa-angle-left right"></i>--}}
{{--                        </p>--}}
{{--                    </a>--}}
{{--                    <ul class="nav nav-treeview">--}}
{{--                        <li class="nav-item">--}}
{{--                            <a href="{!! route('m-sd') !!}" class="nav-link {!! classActiveSegment(2,'m-sd') !!}">--}}
{{--                                <i class="far fa-circle nav-icon"></i>--}}
{{--                                <p>SD</p>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item">--}}
{{--                            <a href="{!! route('m-smp') !!}" class="nav-link {!! classActiveSegment(2, 'm-smp') !!}">--}}
{{--                                <i class="far fa-circle nav-icon"></i>--}}
{{--                                <p>SMP</p>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item">--}}
{{--                            <a href="{!! route('m-smak') !!}" class="nav-link {!! classActiveSegment(2, 'm-smak') !!}">--}}
{{--                                <i class="far fa-circle nav-icon"></i>--}}
{{--                                <p>SMA/K</p>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li class="nav-header">Setting</li>--}}
{{--                <li class="nav-item">--}}
{{--                    <a href="{!! route('setting') !!}" class="nav-link {!! classActiveSegment(1, 'setting') !!}">--}}
{{--                        <i class="nav-icon far fa-calendar-alt"></i>--}}
{{--                        <p>--}}
{{--                            Mata Pelajaran--}}
{{--                        </p>--}}
{{--                    </a>--}}
{{--                </li>--}}
                <li class="nav-header">LABELS</li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon far fa-circle text-danger"></i>
                        <p class="text">Important</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon far fa-circle text-warning"></i>
                        <p>Warning</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon far fa-circle text-info"></i>
                        <p>Informational</p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
