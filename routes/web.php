<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',"CustomLoginController@index")->name("login");

Route::post('auth',"CustomLoginController@auth");
Route::get('logout',"CustomLoginController@logout")->name("logout");

Route::get('coba',"LoginConttroller@demo");
Route::get('coba2',"LoginConttroller@demo2");
Route::get('coba3',"LoginConttroller@demo3");

Route::middleware('custom.session')->group(function () {

    Route::get('dashboard',"DashboardController@index")->name("dashboard");

    Route::get('mahasiswa',"MahasiswaController@index")->name("mahasiswa");
    Route::post('mahasiswa/tambah',"MahasiswaController@tambah")->name("tambah_mahasiswa");
    Route::post('mahasiswa/edit',"MahasiswaController@edit")->name("mahasiswa_edit");
    Route::post('mahasiswa/hapus',"MahasiswaController@hapus")->name("hapus_mahasiswa");
    Route::get('mahasiswa/details/{nik}',"MahasiswaController@view")->name("details_mahasiswa");

    Route::get('tabs/pendidikan/{nik}',"MahasiswaController@tabPendidikan")->name("tabs_pendidikan");
    Route::get('tabs/nilai/{nim}/{semester}',"MahasiswaController@tabNilai")->name("tabs_nilai");
    Route::get('tabs/transkip/{nim}/{semester}',"MahasiswaController@tabTranskip")->name("tabs_transkip");
    Route::get('tabs/profil/{nik}',"MahasiswaController@tabProfil")->name("tabs_profil");

    Route::get('matakuliah',"MatakuliahController@index")->name("matakuliah");
    Route::post('matakuliah/tambah',"MatakuliahController@tambah")->name("tambah_matakuliah");
    Route::post('matakuliah/get',"MatakuliahController@get")->name("get_matakuliah");
    Route::post('matakuliah/edit',"MatakuliahController@edit")->name("edit_matakuliah");
    Route::post('matakuliah/hapus',"MatakuliahController@hapus")->name("hapus_matakuliah");
    Route::get('matakuliah/tb',"MatakuliahController@tb")->name("tb_matakuliah");

    Route::post('pendidikan/tambah',"HistoryPendidikanController@tambah")->name("tambah_pendidikan");
    Route::post('pendidikan/get',"HistoryPendidikanController@get")->name("get_pendidikan");
    Route::post('pendidikan/edit',"HistoryPendidikanController@edit")->name("edit_pendidikan");
    Route::post('pendidikan/hapus',"HistoryPendidikanController@hapus")->name("hapus_pendidikan");
    Route::post('pendidikan/hapus-use-nik',"HistoryPendidikanController@hapusNik")->name("hapus_pendidikan_use_nik ");

    Route::get('prodi',"ProdiController@index")->name("prodi");
    Route::post('prodi/tambah',"ProdiController@tambah")->name("tambah_prodi");
    Route::post('prodi/get',"ProdiController@get")->name("get_prodi");
    Route::post('prodi/edit',"ProdiController@edit")->name("edit_prodi");
    Route::post('prodi/hapus',"ProdiController@hapus")->name("hapus_prodi");
    Route::get('prodi/tb',"ProdiController@tb")->name("tb_prodi");

    Route::get('kelaskuliah',"KelasKuliahController@index")->name("kelaskuliah");
    Route::post('kelaskuliah/tambah',"KelasKuliahController@tambah")->name("tambah_kelaskuliah");
    Route::post('kelaskuliah/get',"KelasKuliahController@get")->name("get_kelaskuliah");
    Route::post('kelaskuliah/edit',"KelasKuliahController@edit")->name("edit_kelaskuliah");
    Route::post('kelaskuliah/hapus',"KelasKuliahController@hapus")->name("hapus_kelaskuliah");
    Route::get('kelaskuliah/tampilQuery/{kode_prodi}/{semester}',"KelasKuliahController@tampilQuery")->name("tampilquery_kelaskuliah");

    Route::get('nilai',"HistoryNilaiController@index")->name("nilai");
    Route::post('nilai/tambah',"HistoryNilaiController@tambah")->name("tambah_nilai");
    Route::post('nilai/get',"HistoryNilaiController@get")->name("get_nilai");
    Route::post('nilai/edit',"HistoryNilaiController@edit")->name("edit_nilai");
    Route::post('nilai/hapus',"HistoryNilaiController@hapus")->name("hapus_nilai");
    Route::post('nilai/hapus-use-kelaskuliah',"HistoryNilaiController@hapusKelaskuliah")->name("hapus_nilai_use_kelaskuliah");
    Route::post('nilai/hapus-use-nim',"HistoryNilaiController@hapusNim")->name("hapus_nilai_use_nim");
    Route::get('nilai/tampilQuery/{kode_prodi}/{semester}',"HistoryNilaiController@tampilQuery")->name("tampilquery_nilai");
});

