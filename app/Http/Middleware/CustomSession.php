<?php

namespace App\Http\Middleware;

use Closure;

class CustomSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(session()->has("auth") && session()->get("auth")) {
            return $next($request);
        }
        return redirect()->route('login');
    }
}
