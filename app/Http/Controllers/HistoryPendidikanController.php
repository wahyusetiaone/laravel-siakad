<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use HelperSoap;
class HistoryPendidikanController extends Controller
{
    public function __construct()
    {
        $this->header = HelperSoap::soapHeader();
        $this->soapPendidikan = HelperSoap::soapPendidikan();
    }

    public function tambah(Request $request){
        $in = $request->all();
        $data = $this->soapPendidikan->tambah_Pendidikan($in,$this->header);
        return response(array('data'=> $data), 200);
    }

    public function get(Request $request){
        $in = $request->nim;
        $key = ['nim'=> $in ];
        $data = $this->soapPendidikan->get_Pendidikan($key,$this->header);
        return response(array('data'=> $data), 200);
    }

    public function edit(Request $request){
        $in = $request->all();
        $data = $this->soapPendidikan->edit_Pendidikan($in,$this->header);
        return response(array('data'=> $data), 200);
    }

    public function hapus(Request $request){
        $in = $request->nim;
        $key = ['nim'=> $in ];
        $data = $this->soapPendidikan->hapus_Pendidikan($key,$this->header);
        return response(array('data'=> $data), 200);
    }

    public function hapusNik(Request $request){
        $in = $request->nik;
        $key = ['nik'=> $in ];
        $data = $this->soapPendidikan->hapus_Pendidikan_Nik($key,$this->header);
        return response(array('data'=> $data), 200);
    }

}
