<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use HelperSoap;

class DashboardController extends Controller
{

    public function __construct()
    {
        $this->header = HelperSoap::soapHeader();
        $this->soapSiakad = HelperSoap::soapSiakad();
    }

    public function index()
    {
        //mhs
        $keyMhs = ['kolom' => 'master_mahasiswa'];
        $mhs = $this->soapSiakad->jumlah_Data($keyMhs,$this->header);
        $arrayMhs = get_object_vars($mhs);
        $sendMhs = array_shift($arrayMhs);
        //prodi
        $keyProdi = ['kolom' => 'master_prodi'];
        $prodi = $this->soapSiakad->jumlah_Data($keyProdi,$this->header);
        $arrayProdi = get_object_vars($prodi);
        $sendProdi = array_shift($arrayProdi);
        //matkul
        $keyMtk = ['kolom' => 'master_matakuliah'];
        $mtk = $this->soapSiakad->jumlah_Data($keyMtk,$this->header);
        $arrayMtk = get_object_vars($mtk);
        $sendMtk = array_shift($arrayMtk);
        return view('dashboard.v1', [
            'mhs'=>$sendMhs,
            'prodi'=>$sendProdi,
            'mtk'=>$sendMtk
        ]);
    }
}
