<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use HelperSoap;

class KelasKuliahController extends Controller
{
    public function __construct()
    {
        $this->header = HelperSoap::soapHeader();
        $this->soapKelasKuliah = HelperSoap::soapKelasKuliah();
        $this->soapMatakuliah = HelperSoap::soapMatakuliah();
        $this->soapProdi = HelperSoap::soapProdi();
    }

    public function index(){
        //getting kelaskuliah
        $data = $this->soapKelasKuliah->tampil_Kelaskuliah(null,$this->header);;
        $array = get_object_vars($data);
        $send = array_shift($array);

        //getting matakuliah
        $matakuliah = $this->soapMatakuliah->tampil_Matakuliah(null,$this->header);;
        $arrayMatakuliah = get_object_vars($matakuliah);
        $sendMatakuliah = array_shift($arrayMatakuliah);

        //getting prodi
        $prodi = $this->soapProdi->tampil_Prodi(null,$this->header);;
        $arrayProdi = get_object_vars($prodi);
        $sendProdi = array_shift($arrayProdi);
        return view('content.kelaskuliah',
            [
                'kk' => $send,
                'mtk' => $sendMatakuliah,
                'pr' => $sendProdi
            ]);
    }

    public function tampilQuery($kode_prodi,$semester){
        $key = [
            'kode_prodi'=> $kode_prodi,
            'semester'=> $semester
            ];
        $data = $this->soapKelasKuliah->query_Kelaskuliah($key,$this->header);
        $array = get_object_vars($data);
        $send = array_shift($array);
        return view('content.frame.tb_kelaskuliah',['kk' =>$send]);
    }

    public function tambah(Request $request){
        $in = $request->all();
        $data = $this->soapKelasKuliah->tambah_Kelaskuliah($in,$this->header);
        return response(array('data'=> $data), 200);
    }

    public function get(Request $request){
        $in = $request->id_kelas_kuliah;
        $key = ['id_kelas_kuliah'=> $in ];
        $data = $this->soapKelasKuliah->get_Kelaskuliah($key,$this->header);
        return response(array('data'=> $data), 200);
    }

    public function edit(Request $request){
        $in = $request->all();
        $data = $this->soapKelasKuliah->edit_Kelaskuliah($in,$this->header);
        return response(array('data'=> $data), 200);
    }

    public function hapus(Request $request){
        $in = $request->id_kelas_kuliah;
        $key = ['id_kelas_kuliah'=> $in ];
        $data = $this->soapKelasKuliah->hapus_Kelaskuliah($key,$this->header);
        return response(array('data'=> $data), 200);
    }
}
