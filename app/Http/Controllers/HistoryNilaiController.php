<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use HelperSoap;

class HistoryNilaiController extends Controller
{
    public function __construct()
    {
        $this->header = HelperSoap::soapHeader();
        $this->soapNilai = HelperSoap::soapNilai();
        $this->soapPendidikan = HelperSoap::soapPendidikan();
        $this->soapProdi = HelperSoap::soapProdi();
        $this->soapKelasKuliah = HelperSoap::soapKelasKuliah();
    }

    public function index(){
        //getting Nilai
        $data = $this->soapNilai->tampil_Nilai(null,$this->header);;
        $array = get_object_vars($data);
        $send = array_shift($array);

        //getting mahasiswa dari pendidikan karena ada nim
        $mahasiswa = $this->soapPendidikan->tampil_Pendidikan(null,$this->header);;
        $arrayMahasiswa = get_object_vars($mahasiswa);
        $sendMahasiswa = array_shift($arrayMahasiswa);


        //getting prodi
        $prodi = $this->soapProdi->tampil_Prodi(null,$this->header);;
        $arrayProdi = get_object_vars($prodi);
        $sendProdi = array_shift($arrayProdi);

        //getting kelaskuliah
        $kelas = $this->soapKelasKuliah->tampil_Kelaskuliah(null,$this->header);;
        $arrayKelas = get_object_vars($kelas);
        $sendKelas = array_shift($arrayKelas);

        return view('content.nilai',
            [
                'nilai' => $send,
                'mhs' => $sendMahasiswa,
                'pr' => $sendProdi,
                'kk' => $sendKelas
            ]);
    }

    public function tampilQuery($kode_prodi,$semester){
        $key = [
            'kode_prodi'=> $kode_prodi,
            'semester'=> $semester
        ];
        $data = $this->soapNilai->query_Nilai($key,$this->header);
        $array = get_object_vars($data);
        $send = array_shift($array);
        return view('content.frame.tb_nilai',['nilai' =>$send]);
    }

    public function tambah(Request $request){
        $in = $request->all();
        $data = $this->soapNilai->tambah_Nilai($in,$this->header);
        return response(array('data'=> $data), 200);
    }

    public function get(Request $request){
        $in = $request->id_history_nilai;
        $key = ['id_history_nilai'=> $in ];
        $data = $this->soapNilai->get_Nilai($key,$this->header);
        return response(array('data'=> $data), 200);
    }

    public function edit(Request $request){
        $in = $request->all();
        $data = $this->soapNilai->edit_Nilai($in,$this->header);
        return response(array('data'=> $data), 200);
    }

    public function hapus(Request $request){
        $in = $request->id_history_nilai;
        $key = ['id_history_nilai'=> $in ];
        $data = $this->soapNilai->hapus_Nilai($key,$this->header);
        return response(array('data'=> $data), 200);
    }

    public function hapusKelasKuliah(Request $request){
        $in = $request->id_kelas_kuliah;
        $key = ['id_kelas_kuliah'=> $in ];
        $data = $this->soapNilai->hapus_Nilai_Kelaskuliah($key,$this->header);
        return response(array('data'=> $data), 200);
    }

    public function hapusNIm(Request $request){
        $in = $request->nim;
        $key = ['nim'=> $in ];
        $data = $this->soapNilai->hapus_Nilai_Nim($key,$this->header);
        return response(array('data'=> $data), 200);
    }
}
