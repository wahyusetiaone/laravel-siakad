<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use SoapClient;
use SoapHeader;

class MatakuliahController extends Controller
{
    public function __construct()
    {
        $apiauth =array('UserName'=>'username','Password'=>'password');
        $wsdl = 'http://hanif.so:8080//siakad2019/Matakuliah?wsdl';

        $this->soap = new SoapClient($wsdl);
        $this->header = new SoapHeader('http://hanif.so:8080/', 'AuthHeader', $apiauth);
        $this->soap->__setSoapHeaders($this->header);
    }

    public function index(){
        $data = $this->soap->tampil_Matakuliah(null,$this->header);
        $array = get_object_vars($data);
        $send = array_shift($array);
        return view('content.matakuliah',['mt' =>$send]);
    }

    public function tb(){
        $data = $this->soap->tampil_Matakuliah(null,$this->header);
        $array = get_object_vars($data);
        $send = array_shift($array);
        return view('content.frame.tb_matakuliah',['mt' =>$send]);
    }

    public function tambah(Request $request){
        $in = $request->all();
        $data = $this->soap->tambah_Matakuliah($in,$this->header);
        return response(array('data'=> $data), 200);
    }

    public function get(Request $request){
        $in = $request->kode_matakuliah;
        $key = ['kode_matakuliah'=> $in ];
        $data = $this->soap->get_Matakuliah($key,$this->header);
        return response(array('data'=> $data), 200);
    }

    public function edit(Request $request){
        $in = $request->all();
        $data = $this->soap->edit_Matakuliah($in,$this->header);
        return response(array('data'=> $data), 200);
    }

    public function hapus(Request $request){
        $in = $request->kode_matakuliah;
        $key = ['kode_matakuliah'=> $in ];
        $data = $this->soap->hapus_Matakuliah($key,$this->header);
        return response(array('data'=> $data), 200);
    }

}
