<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use SoapClient;
use SoapHeader;

class ProdiController extends Controller
{
    public function __construct()
    {
        $apiauth =array('UserName'=>'username','Password'=>'password');
        $wsdl = 'http://hanif.so:8080//siakad2019/Prodi?wsdl';

        $this->soap = new SoapClient($wsdl);
        $this->header = new SoapHeader('http://hanif.so:8080/', 'AuthHeader', $apiauth);
        $this->soap->__setSoapHeaders($this->header);
    }

    public function index(){
        return view('content.prodi');
    }

    public function tb(){
        $data = $this->soap->tampil_Prodi(null,$this->header);;
        $array = get_object_vars($data);
        $send = array_shift($array);
        return view('content.frame.tb_prodi',['pr' =>$send]);
    }

    public function tambah(Request $request){
        $in = $request->all();
        $data = $this->soap->tambah_Prodi($in,$this->header);
        return response(array('data'=> $data), 200);
    }

    public function get(Request $request){
        $in = $request->kode_prodi;
        $key = ['kode_prodi'=> $in ];
        $data = $this->soap->get_Prodi($key,$this->header);
        return response(array('data'=> $data), 200);
    }

    public function edit(Request $request){
        $in = $request->all();
        $data = $this->soap->edit_Prodi($in,$this->header);
        return response(array('data'=> $data), 200);
    }

    public function hapus(Request $request){
        $in = $request->kode_prodi;
        $key = ['kode_prodi'=> $in ];
        $data = $this->soap->hapus_Prodi($key,$this->header);
        return response(array('data'=> $data), 200);
    }
}
