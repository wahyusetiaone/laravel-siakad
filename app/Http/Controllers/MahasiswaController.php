<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use HelperSoap;

class MahasiswaController extends Controller
{
    public function __construct()
    {
        $this->header = HelperSoap::soapHeader();
        $this->soapMahasiswa = HelperSoap::soapMahasiswa();
        $this->soapPendidikan = HelperSoap::soapPendidikan();
        $this->soapNilai = HelperSoap::soapNilai();
        $this->soapProdi = HelperSoap::soapProdi();
    }

    public function index()
    {
        $data = $this->soapMahasiswa->tampil_Mahasiswa(null,$this->header);
        $array = get_object_vars($data);
        $send = array_shift($array);
        $dataP = $this->soapProdi->tampil_Prodi(null,$this->header);
        $arrayP = get_object_vars($dataP);
        $sendP = array_shift($arrayP);
        return view('content.mahasiswa',[
            'mhs' =>$send,
            'allprodi' => $sendP
        ]);
    }

    public function tambah(Request $request){
        $in = $request->all();
        $data = $this->soapMahasiswa->tambah_Mahasiswa($in,$this->header);
        return response(array('data'=> $data), 200);
    }

    public function edit(Request $request){
        $in = $request->all();
        $data = $this->soapMahasiswa->edit_Mahasiswa($in,$this->header);
        return response(array('data'=> $data), 200);
    }

    public function hapus(Request $request){
        $in = $request->nik;
        $key = ['nik'=> $in ];
        $data = $this->soapMahasiswa->hapus_Mahasiswa($key,$this->header);
        return response(array('data'=> $data), 200);
    }

    public function view($nik){
        //details
        $key = ["nik"=>$nik];
        $data = $this->soapMahasiswa->get_Mahasiswa($key,$this->header);
        $array = get_object_vars($data);
        $send = array_shift($array);
        //nilai
        $nilai = $this->soapNilai->cari_Nilai(["key"=>$send[0]],$this->header);
        $arrayNilai = get_object_vars($nilai);
        $sendNilai = array_shift($arrayNilai);
        //allmhs
        $dataM = $this->soapMahasiswa->tampil_Mahasiswa(null,$this->header);
        $arrayM = get_object_vars($dataM);
        $sendM = array_shift($arrayM);
        //allProdi
        $dataP = $this->soapProdi->tampil_Prodi(null,$this->header);
        $arrayP = get_object_vars($dataP);
        $sendP = array_shift($arrayP);
        return view('content.details_mahasiswa',[
            'mhs' =>$send,
            'nilai' => $sendNilai,
            'allmhs' => $sendM,
            'allprodi' => $sendP
        ]);
    }

    public function tabPendidikan($nik){
        $key = ["nik"=>$nik];
        $data = $this->soapPendidikan->query_Pendidikan($key,$this->header);
        $array = get_object_vars($data);
        $send = array_shift($array);
        return view('content.tabs.pendidikan',['mhs' =>$send]);
    }

    public function tabNilai($nim,$semester){
        $key = [
            "nim"=>$nim,
            "semester"=>$semester
        ];
        $data = $this->soapNilai->query_CariNilai($key,$this->header);
        $array = get_object_vars($data);
        $send = array_shift($array);
        return view('content.tabs.nilai',['nilai'=>$send]);
    }

    public function tabTranskip($nim,$semester){
        $key = [
            "nim"=>$nim,
            "semester"=>$semester
        ];
        $data = $this->soapNilai->query_CariNilai($key,$this->header);
        $array = get_object_vars($data);
        $send = array_shift($array);
        return view('content.tabs.transkip',['transkip'=>$send]);

    }

    public function tabProfil($nik){
        //m
        $keyMahasiswa = [
            "nik"=>$nik
        ];
        $dataMahasiswa = $this->soapMahasiswa->get_Mahasiswa($keyMahasiswa,$this->header);
        $arrayMahasiswa = get_object_vars($dataMahasiswa);
        $sendMahasiswa = array_shift($arrayMahasiswa);

        return view('content.tabs.profil',[
            'dataM'=>$sendMahasiswa
        ]);
    }
}
