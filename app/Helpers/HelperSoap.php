<?php

namespace App\Helpers;

use Illuminate\Http\Request;
use SoapClient;
use SoapHeader;

ini_set('soap.wsdl_cache_enabled', 0);
ini_set('soap.wsdl_cache_ttl', 900);
ini_set('default_socket_timeout', 15);

class HelperSoap{

    public static function soapHeader()
    {
        $apiauth =array('UserName'=>'username','Password'=>'password');
        $header = new SoapHeader('http://hanif.so:8080/', 'AuthHeader', $apiauth );

        return $header;
    }

    public static function soapSiakad()
    {
        $wsdl = 'http://hanif.so:8080//siakad2019/Siakad?wsdl';

        $soap = new SoapClient($wsdl);
        $header = self::soapHeader();
        $soap->__setSoapHeaders($header);
        return $soap;
    }

    public static function soapKelasKuliah()
    {
        $wsdl = 'http://hanif.so:8080//siakad2019/Kelas?wsdl';

        $soap = new SoapClient($wsdl);
        $header = self::soapHeader();
        $soap->__setSoapHeaders($header);
        return $soap;
    }

    public static function soapMatakuliah()
    {
        $wsdl = 'http://hanif.so:8080//siakad2019/Matakuliah?wsdl';

        $soap = new SoapClient($wsdl);
        $header = self::soapHeader();
        $soap->__setSoapHeaders($header);
        return $soap;
    }

    public static function soapProdi()
    {
        $wsdl = 'http://hanif.so:8080//siakad2019/Prodi?wsdl';

        $soap = new SoapClient($wsdl);
        $header = self::soapHeader();
        $soap->__setSoapHeaders($header);
        return $soap;
    }

    public static function soapNilai()
    {
        $wsdl = 'http://hanif.so:8080//siakad2019/Nilai?wsdl';

        $soap = new SoapClient($wsdl);
        $header = self::soapHeader();
        $soap->__setSoapHeaders($header);
        return $soap;
    }

    public static function soapMahasiswa()
    {
        $wsdl = 'http://hanif.so:8080//siakad2019/Master_Mahasiswa?wsdl';

        $soap = new SoapClient($wsdl);
        $header = self::soapHeader();
        $soap->__setSoapHeaders($header);
        return $soap;
    }

    public static function soapPendidikan()
    {
        $wsdl = 'http://hanif.so:8080//siakad2019/Pendidikan?wsdl';

        $soap = new SoapClient($wsdl);
        $header = self::soapHeader();
        $soap->__setSoapHeaders($header);
        return $soap;
    }

}
